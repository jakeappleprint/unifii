/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 31);
/******/ })
/************************************************************************/
/******/ ({

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(32);


/***/ }),

/***/ 32:
/***/ (function(module, exports) {

(function(blocks, element, blockEditor, components ){
  var el = element.createElement 
  var InnerBlocks = blockEditor.InnerBlocks;
  var MediaUploadCheck = blockEditor.MediaUploadCheck;
  var MediaUpload = blockEditor.MediaUpload;
  var InspectorControls = blockEditor.InspectorControls;
  var PanelBody = components.PanelBody;
  var PanelRow = components.PanelRow;
  var Button = components.Button;

  var OB_TEMPLATE = [
    ['core/group', {}, [
      ['core/heading', { level: 3}],
      ['core/paragraph', {}] ]] ]


  blocks.registerBlockType('uni/overlay-block', {
      title: 'Unifii Banner Block',
      icon: 'feedback',
      category: 'common', 
      attributes: {
       backgroundCss: {
         type: 'string',
         default: null,
       },
     },

      edit: function(props){

        var updateBGImage = function(value){
          props.setAttributes({ backgroundCss: value.sizes.full.url })
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'Background Image'},
              el(PanelRow, {},
                el(MediaUploadCheck, {}, 
                  el(MediaUpload, {
                    multiple: false,
                    type: 'image',
                    value: props.attributes.backgroundID,
                    onSelect: updateBGImage,
                    render: function (open) {
                      return el(Button, { 
                        onClick: open.open,
                        className: 'is-default is-large is-button',
                      }, 'Choose Background Image')
                    },
                  })
                )
              )
            )
          ),
          el( 'div', { 
              className: props.className,
              style: {
                backgroundImage: 'url('+ props.attributes.backgroundCss +')',
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                color: '#fff',
              },
            },
            el( InnerBlocks, {              
                template: OB_TEMPLATE,
                templateLock: false,
            })
          ) ]
    },
    save: function(props){
        var cN = props.className + ' overlay-banner blur-image'
        return el( 'div', {
            className: cN,

          },
          el('div', { className: 'wrap' },
            el('div', {
              className: 'background-image',
              style: {
                backgroundImage: 'url('+ props.attributes.backgroundCss +')',
              },
            }),
            el(InnerBlocks.Content, {})
          )
        )
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
  window.wp.components
))


/***/ })

/******/ });
//# sourceMappingURL=overlay-block.js.map