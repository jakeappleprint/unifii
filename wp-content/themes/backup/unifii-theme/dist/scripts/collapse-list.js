/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 39);
/******/ })
/************************************************************************/
/******/ ({

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(40);


/***/ }),

/***/ 40:
/***/ (function(module, exports) {

(function(blocks, blockEditor, element, components ){
  var el = element.createElement 
  var InnerBlocks = blockEditor.InnerBlocks;
  var InspectorControls = blockEditor.InspectorControls;
  var PanelBody = components.PanelBody;
  var PanelRow = components.PanelRow;
  var TextControl = components.TextControl;

  var CL_TEMPLATE = [ 
    ['uni/collapse-block', {}],
    ['uni/collapse-block', {}] ]

  blocks.registerBlockType('uni/collapse-list', {
      title: 'Unifii Collapse List',
      icon: 'list-view',
      category: 'common',
      supports: { 
        inserter: false,
      },
      attributes: {
        collapseTitle: {
          type: 'string',
          default: null,
        },
      },
      edit: function(props){
        var updateTitle = function(v){
          props.setAttributes({collapseTitle: v})
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'List Title'},
              el(PanelRow, {},
                el(TextControl, {
                  value: props.attributes.collapseTitle,
                  onChange: updateTitle,
                })
              )
            )
          ),
          el('div', { className: props.className },
            el('h5', { className: 'collapse-list-title'},  props.attributes.collapseTitle),
            el( InnerBlocks, {              
                template: CL_TEMPLATE,
                allowedBlocks: ['uni/collapse-block'],
            })
          ) ]
    },
    save: function(props){
      return el('div', {}, 
          el('h5', { className: 'collapse-list-title grab-title'},  props.attributes.collapseTitle),
          el(InnerBlocks.Content, {})
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element,
  window.wp.components
))


/***/ })

/******/ });
//# sourceMappingURL=collapse-list.js.map