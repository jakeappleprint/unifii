/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 37);
/******/ })
/************************************************************************/
/******/ ({

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(38);


/***/ }),

/***/ 38:
/***/ (function(module, exports) {

(function(blocks, blockEditor, element){
  var el = element.createElement
  var InnerBlocks = blockEditor.InnerBlocks;

  var CS_TEMPLATE = [ 
    ['uni/collapse-list', { focus: true }],
    ['uni/collapse-list', {}] ]

  blocks.registerBlockType('uni/collapse-section', {
      title: 'Unifii Collapse Section',
      icon: 'editor-justify',
      category: 'common',
      edit: function(props){
        return el( 'div', { className: props.className },
          el( InnerBlocks, {              
              template: CS_TEMPLATE,
              allowedBlocks: ['uni/collapse-list'],
          })
        )
    },
    save: function(){
      return el('div', { className: 'wrap'}, 
        el('div', { className: 'container' },
          el('ul', { 
            id: 'collapseNav',
            className: 'collapse-nav nav nav-fill create-nav',
          }), 
          el(InnerBlocks.Content, {}),
          el('p', { className: 'link-box' }, [ 
            'Can\'t find what you were looking for?',
             el('a', { 
                href:'/contact',
                className: 'button',
              },
              ['Get in touch']
            ) ])
        )
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element 
))


/***/ })

/******/ });
//# sourceMappingURL=collapse-section.js.map