/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(36);


/***/ }),

/***/ 36:
/***/ (function(module, exports) {

(function(blocks, blockEditor, element, components ){
  var el = element.createElement 
  var InnerBlocks = blockEditor.InnerBlocks;
  var InspectorControls = blockEditor.InspectorControls;
  var PanelBody = components.PanelBody;
  var PanelRow = components.PanelRow;
  var TextControl = components.TextControl;

  var IL_TEMPLATE = [ 
    ['core/list', { className: 'no-styles' }] ]

  blocks.registerBlockType('uni/info-list', {
      title: 'Unifii info List',
      icon: 'list-view',
      category: 'common',
      supports: { 
        inserter: false,
      },
      attributes: {
        infoTitle: {
          type: 'string',
          default: 'Enter List Title',
        },
      },
      edit: function(props){
        var updateTitle = function(v){
          props.setAttributes({infoTitle: v})
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'List Title'},
              el(PanelRow, {},
                el(TextControl, {
                  value: props.attributes.infoTitle,
                  onChange: updateTitle,
                })
              )
            )
          ),
          el('div', { className: props.className },
            el('h5', { className: 'info-list-title'},  props.attributes.infoTitle),
            el( InnerBlocks, {              
                template: IL_TEMPLATE,
                templateLock: true,
            })
          ) ]
    },
    save: function(props){
      return el('div', {}, 
          el('h5', { className: 'info-list-title'},  props.attributes.infoTitle),
          el(InnerBlocks.Content, {})
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element,
  window.wp.components
))


/***/ })

/******/ });
//# sourceMappingURL=info-list.js.map