/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 29);
/******/ })
/************************************************************************/
/******/ ({

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(30);


/***/ }),

/***/ 30:
/***/ (function(module, exports) {

(function(blocks, element, blockEditor ){
  var el = element.createElement 
  var InnerBlocks = blockEditor.InnerBlocks;

  var SL_TEMPLATE = [
    ['core/image', { sizeSlug: 'image-slider' }],
    ['core/image', { sizeSlug: 'image-slider' }],
    ['core/image', { sizeSlug: 'image-slider' }] ]


  blocks.registerBlockType('uni/uni-slider', {
      title: 'Unifii Image Slider',
      icon: 'images-alt2',
      category: 'common', 
      edit: function(){
        return el( 'div', {},
            el( InnerBlocks, {              
                template: SL_TEMPLATE,
                allowedBlocks: ['core/image'],
            })
          )
    },
    save: function(){
        return el( 'div', { id: 'uni-slider' },
          el('div', { id: 'uni-slider-frame', className: 'frame' },
            el('div', { id: 'uni-slider-rail', className: 'rail image-slider-rail'},
              el(InnerBlocks.Content, {})
            )
          ),
          el('div', { 
            id: 'uni-sl-lnav',
            className: 'uni-nav uni-left-nav',
          },
          el('span',{},
            el('i', { className: 'fa fa-chevron-left' })
          )
        ),
        el('div', { 
            id: 'uni-sl-rnav',
            className: 'uni-nav uni-right-nav',
          },
          el('span',{},
            el('i', { className: 'fa fa-chevron-right' })
          )
        )
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
  window.wp.components
))


/***/ })

/******/ });
//# sourceMappingURL=uni-slider.js.map