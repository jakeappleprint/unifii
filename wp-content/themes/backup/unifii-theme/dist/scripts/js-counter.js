/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/unifii-theme/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 25);
/******/ })
/************************************************************************/
/******/ ({

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(26);


/***/ }),

/***/ 26:
/***/ (function(module, exports) {

(function(blocks, editor, element){
    var el = element.createElement


    blocks.registerBlockType('uni/js-counter', {
        title: 'Count Upwards',
        icon: 'upload',
        category: 'common',
        attributes: {
            content: {
                type: 'string',
                attribute: 'value',
            },
            label: {
                type: 'string',
                attribute: 'value',
            },
        },
        edit: function(props){
            function updateContent(ev) {
                props.setAttributes({ content: ev.target.value })
            }
            function updateLabel(ev) {
                props.setAttributes({ label: ev.target.value })
            }            
            return el(
                'div',
                {},
                el(
                    'input',
                    { 
                        className: props.className,
                        placeholder: '00',
                        onChange: updateContent,
                        value: props.attributes.content,
                        type: 'text',
                    }
                ),
                el(
                    'input',
                    {
                        className: props.className,
                        placeholder: 'Counter Label',
                        onChange: updateLabel,
                        value: props.attributes.label,
                        type: 'text',
                    }
                ),
                el(
                    'div',
                    { className: props.className },
                    el(
                        'span',
                        {},
                        props.attributes.content
                    ),
                    el(
                        'p',
                        { className: 'counter-label' },
                        props.attributes.label
                    )
                )                
            )
        },
        save: function(props){
            return el(
                'div',
                { className: props.className },
                el(
                    'span',
                    {},
                    props.attributes.content
                ),
                el(
                    'p',
                    { className: 'counter-label' },
                    props.attributes.label
                )
            )
        },
    })
}(
    window.wp.blocks,
    window.wp.editor,
    window.wp.element    
))

/***/ })

/******/ });
//# sourceMappingURL=js-counter.js.map