<?php

namespace App\Options;

/**
 * Theme setup
 */

$theme_options = new GO_Theme_Options;

class GO_Theme_Options {

	function __construct() {
		add_action( 'admin_init', array( $this,'un48_theme_options_init' ) );
		add_action( 'admin_menu', array( $this, 'add_un48_menu_option' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'add_un48_admin_scripts' ), 100 );
		add_action( 'wp_enqueue_scripts', array( $this, 'add_un48_inline_styles') , 200 );
	}

	function add_un48_menu_option() {
		add_options_page( get_bloginfo('title') .' Theme Options', get_bloginfo('title') .' Theme Options', 'manage_options', 'un48_theme_options', array($this, 'un48_theme_options_display') );
	}

	function add_un48_admin_scripts($hook) {	
		if( is_admin() && $hook == 'settings_page_un48_theme_options' ) {
			$settings = wp_enqueue_code_editor( array( 'type' => 'text/css' ) );
			wp_add_inline_script(
			    'code-editor',
			    sprintf(
			        'jQuery( function() { wp.codeEditor.initialize( "inline_css", %s ); } );',
			        wp_json_encode( $settings )
			    )
			);
		} 	
	}

	function add_un48_inline_styles(){
		$css = get_v('inline_css');
		if( !empty($css) ) {
			wp_add_inline_style('sage/main.css',wp_strip_all_tags($css));
		}

		if(!empty(get_v('fontawesome_code'))) {
			wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/' . get_v('fontawesome_code') . '.js', false, null);
		}

		$typekit_url = get_v('typekit_url');
		if(!empty($typekit_url)) {
			wp_enqueue_style('typekit', $typekit_url, false, null);
		}		
	}

	function un48_theme_options_display() {
	?>
		<div class="wrap">
		<h2>Theme Options</h2>
		<form action="options.php" method="post">
			<?php settings_fields('un48_theme_options'); ?>
			<?php do_settings_sections('un48_basic-options'); ?>
			<?php do_settings_sections('un48_menu_options'); ?>
			<?php do_settings_sections('un48_layout-options'); ?>
			<div id="social-section">
				<?php do_settings_sections( 'un48_theme-social' ); ?>
			</div>	
			<?php do_settings_sections( 'un48_configuration' ); ?>	
			<p class="submit">
				<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
			</p>
		</form>
		</div>
		<script type="text/javascript">
	 	/* <![CDATA[ */
		 	(function( $ ) {
				$(function() {
					function socialCheck(t){
						if(t.prop('checked') === true) {
							$('#social-section').slideDown();
						} else {
							$('#social-section').slideUp();
						}			
					}
					socialCheck($('#enable_social'));
					$('#enable_social').change(function(){
						socialCheck($(this));
					});					
				}); 
			})( jQuery );
		/* ]]> */
		</script>
	<?php 
	}	

	function theme_options(){
		$un48_opts = array(
			'un48_basic-options' => array(
				'name' 		=> 'Company Details',
				'fields'	=> array(
					'company_tel' => array(
						'name'	=> 'Telephone Number',
					),
					'company_email' => array(
						'name'	=> 'Email Address',
						'func'	=> array($this, 'email_input'),
					),								
					'company_address' => array(
						'name'	=> 'Address',
						'func'	=> array( $this, 'txtarea_input' ),
						'rows'	=> 8,
						'cols'	=> 42,
					),
					'company_logo' => array(
						'name' => 'Logo ID',
						'func' => array($this, 'numput'),
					),


	/*					'alt_logo'	=> array(
						'name'	=> 'Inverted Logo ID',
						'func'	=> array($this, 'numput'),
					),
					'mobile_logo'	=> array(
						'name'	=> 'Mobile Devices Logo ID',
						'func'	=> array($this, 'numput'),
					),

					'company_registration' => array(
						'name'	=> 'Company Reg No',
					),
					'map_address' => array(
						'name'	=> 'Map Location',
					),
													
 */
				)
			),
			'un48_menu_options' => array(
				'name' => 'Mega Menu Text Content',
				'fields' => array(
					'un48_services_txt' => array(
						'name' => 'Text for Service Dropdown',
					),
					'un48_solutions_txt' => array(
						'name' => 'Text for Solutions Dropdown',
					),
					'un48_resources_txt' => array(
						'name' => 'Text for Resources Dropdown',
					),
					'un48_about_txt' => array(
						'name' => 'Text for About Dropdown',
					),										
				),
			),
			'un48_layout-options' => array(
				'name'	=> 'Theme Layout Options',		
				'fields'	=> array(
					'get_in_touch' => array(
						'name' => 'Get In Touch Page',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),
					'main_contact' => array(
						'name' => 'Contact Form Page',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),	
/*					'free_quote'	=> array(
						'name'	=> 'Free Quote Page',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),
					'free_quote_form'	=> array(
						'name'	=> 'Free Quote Form',
					),
				
					'arrange_visit' => array(
						'name' => 'Arrange Visit Page',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),
					'why_partner' => array(
						'name' => 'Why partner with us content',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),					
					'footer_content' => array(
						'name' => 'Footer Content',
						'func'	=> array($this, 'admin_page_dropdown_options'),
					),		*/				
				),
			),
			'un48_configuration' => array(
				'name'		=> 'Theme Config Stuff',
				'fields'	=> array(
					'fontawesome_code' => array(
						'name' => 'FontAwesome ID code',
					),
					'google_mapkey'	=> array(
						'name' => 'Google Map Embed API Key',
					),
					'google_analytics' => array(
						'name' => 'Google Analytics Publisher ID',
					),
					'typekit_url' => array(
						'name' => 'Typekit URL',
					),
					'inline_css' => array(
						'name'	=> 'Above The Fold CSS (<em>minimised</em>)',
						'func'	=> array($this, 'css_textarea'),
					), 
				),
			),		
		);	
		return $un48_opts;	
	}

	function un48_theme_options_init(){
		register_setting( 'un48_theme_options', 'un48_theme_opts', array( 'sanitize_callback' => array( $this, 'un48_opts_validation') ) );
		$opts = $this->theme_options();
		foreach( $opts as $k => $section ) :
			$func = array( $this, 'txt_input' );
			$section_id = str_replace( '-', '_', $k );
			add_settings_section( $section_id, $section['name'], null,  $k );
			foreach( $section['fields'] as $id => $data ) :				
				if( !isset( $data['func'] ) ) {
					$f = $func;
					$data['size'] = 40;
				} else {
					$f = $data['func'];
				}	
				$s = array(
					'name' => 'un48_theme_opts[' . $id . ']',
					'id' => $id, 					
				);	
				$o = array('size', 'opts', 'rows', 'cols');
				foreach( $data as $key => $val ) :
					if( in_array( $key, $o ) ) {
						$s[ $key ] = $val;
					}
				endforeach;						
				add_settings_field( $id, $data['name'], $f, $k, $section_id, $s );
			endforeach;
		endforeach;
	}

	function un48_opts_validation($fields){
		return $fields;
	}

	function all_my_selects($args){
		$data = $args['opts'];
		unset($args['opts']);
		$v = $this->get_v( $args['id'] );
		if( isset( $data ) ) {  ?>
			<select <?php echo $this->get_atts($args); ?> class="nnm-select">
				<?php foreach( $data as $key => $val) :	?>
					<option value="<?php echo $key; ?>" <?php if( $v == $key ) { echo 'selected'; } ?>><?php echo $val; ?></option>
					<?php 
				endforeach; ?>
			</select>					
		<?php } else {
			echo '<p>' . __( 'No arguments available', 'nnmcs' ) . '</p>';
		}
	}

	function chk_options($args){
		$value = $this->get_v( $args['id'] );	
		if(1 == $value) { 
			$value='checked'; 
		}
		echo '<input type="checkbox" ' . $this->get_atts($args) . ' value="1" '.$value.' />';
	}

	function numput($args){
		$v = $this->get_v( $args['id'] );
		echo '<input type="number" step="1" min="0" ' . $this->get_atts($args) . ' value="' . $v . '" />';
	}

	function txt_input($args){
		$v = $this->get_v( $args['id'] );	
		echo '<input type="text" class="regular-text" ' . $this->get_atts($args) . ' value="'. $v .'" />';
	}

	function url_input($args){
		$v = $this->get_v( $args['id'] );	
		echo '<input type="url" class="regular-text code" ' . $this->get_atts($args) . ' value="'. $v .'" />';
	}

	function txtarea_input($args){
		$v = $this->get_v( $args['id'] );
		echo '<textarea ' . $this->get_atts($args) . '>' . $v . '</textarea>';
	}

	function email_input($args){
		$v = $this->get_v( $args['id'] );	
		echo '<input type="email" ' . $this->get_atts($args) . ' value="'. $v .'"  class="regular-text" />';
	}	

	function admin_page_dropdown_options($args){
		$page_args = array(
			'post_status'		=> 'private,publish',
			'posts_per_page'	=> -1,
		);
		$pages = get_pages($page_args);
		if( $pages ) { 		
			$value = $this->get_v( $args['id'] ); ?>
			<select <?php echo $this->get_atts($args); ?> class="page-selector">
				<option value="">No Page Selected</option>
				<?php foreach($pages as $p) { 
					$s = '';
					if($p->ID == $value) { $s = 'selected'; } 
					echo '<option value="' .  $p->ID . '" ' . $s . '>' . $p->post_title . '</option>';
				} 	?>
			</select>
		<?php } else { ?>
			<p><em>no pages! :(</em></p>
		<?php }
	}

	function css_textarea($args){
		$v = $this->get_v( $args['id'] );
		$a = array(
			'wpautop'			=> false,
			'media_buttons'		=> false,
			'textarea_name' 	=> $args['name'],
			'tinymce'			=> false,
			'quicktags'			=> false,
		);
		
		wp_editor( esc_html($v), $args['id'], $a );
	}

	function get_v($v){
		$opts = get_option('un48_theme_opts');
		if( isset( $opts[ $v ] ) ) {
			return esc_attr($opts[ $v ]);
		} else {
			return;
		}
	}

	function get_atts($args){
		$attrs = false;
		if(is_array($args)) {
			$attrs = '';
			foreach( $args as $k => $v ) {
				$attrs .= $k . '="' . $v . '"';
			}		
		}
		return $attrs;
	}
}

function get_v($v){
	$opts = get_option('un48_theme_opts');
	if( isset( $opts[ $v ] ) ) {
		return $opts[ $v ];
	} else {
		return;
	}
}

add_action('wp_footer', function (){ 
	$analytics = get_v('google_analytics');
	if( !empty( $analytics ) ) { ?>
		<script src="https://cdn.jsdelivr.net/ga-lite/latest/ga-lite.min.js" async></script>
		<script>
			var galite = galite || {};
			galite.UA = '<?= $analytics; ?>';
		</script>
	<?php }
}, 99);

