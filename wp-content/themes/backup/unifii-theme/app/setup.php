<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_script('gator', 'https://t.gatorleads.co.uk/Scripts/ssl/1378aef5-cc68-4464-89ef-dcfefd601f9d.js', [], null, true);
    wp_localize_script('sage/main.js', 'themescripts',['ajaxurl' => admin_url( 'admin-ajax.php' ) ]);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'sub_nav' => 'Sub footer quick links',
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    add_image_size('post-thumb', 500, 240, ['center','center']);
    add_image_size('square', 555, 555, ['center','center']);
    add_image_size('case-study', 1100, 540, ['center', 'center']);
    add_image_size('image-slider', 1280, 727, ['center', 'center']);
    add_image_size('letterbox', 1920, 420, ['center','center']);
	add_image_size('slide', 2560, 978, ['center', 'center']);

    add_theme_support( 'custom-header' );


    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register Post Types & Gutenberg Blocks
**/
add_action('init', function () {
	register_post_type('un48_slides',
		array(
			'labels' => array(
				'name' => __( 'Slides' ),
				'singular_name' => __( 'Slide' ),
				'add_new_item' => 'Add Slide',
				'new_item' => 'New Slide',
				'edit_item' => 'Edit Slide',
			),
			'description' => get_bloginfo('title') . ' Slides',
			'public' => false,
			'has_archive' => false,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'show_in_nav_menus' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_rest'	=> true,
			'menu_position' => 2,  
			'menu_icon' => 'dashicons-format-gallery',
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
			),
		)
    );	
	register_post_type('un48_services',
		array(
			'labels' => array(
				'name' => __( 'Services' ),
				'singular_name' => __( 'Service' ),
				'add_new_item' => 'Add Service',
				'new_item' => 'New Service',
				'edit_item' => 'Edit Service',
			),
			'description' => get_bloginfo('title') . ' Services',
			'public' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_rest'	=> true,
			'menu_position' => 4,  
			'menu_icon' => 'dashicons-admin-generic',
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
				'revisions',
				'page-attributes',
			),
			'rewrite' => array('slug' => 'services'),
		)
    );

	register_post_type('un48_solutions',
		array(
			'labels' => array(
				'name' => __( 'Solutions' ),
				'singular_name' => __( 'Solution' ),
				'add_new_item' => 'Add Solution',
				'new_item' => 'New Solution',
				'edit_item' => 'Edit Solution',
			),
			'description' => get_bloginfo('title') . ' Solutions',
            'public' => true,
            'hierarchical' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_rest'	=> true,
			'menu_position' => 8,  
			'menu_icon' => 'dashicons-awards',
			'supports' => array(
				'title',
				'editor',
                'revisions',
				'thumbnail',
                'excerpt',
                'page-attributes'
			),
			'rewrite' => array('slug' => 'solutions'),
		)
    );

	register_post_type('un48_casestudies',
		array(
			'labels' => array(
				'name' => __( 'Case Studies' ),
				'singular_name' => __( 'Case Study' ),
				'add_new_item' => 'Add Case Study',
				'new_item' => 'New Case Study',
				'edit_item' => 'Edit Case Study',
			),
			'description' => get_bloginfo('title') . ' Case Studies',
			'public' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_rest'	=> true,
			'menu_position' => 8,  
			'menu_icon' => 'dashicons-awards',
			'supports' => array(
				'title',
				'editor',
                'revisions',
				'thumbnail',
				'excerpt',
            ),
            'taxonomies' => ['category'],
			'rewrite' => array('slug' => 'case-study'),
		)
    );

    // Gutenberg assets
   wp_register_style('uni-gutenberg-editor', asset_path('styles/editor.css'), array('wp-edit-blocks'), null);

    wp_register_script('uni-overlay', asset_path('scripts/overlay-block.js'), array( 'wp-blocks', 'wp-element', 'wp-block-editor', 'wp-components' ), null );
    register_block_type( 'uni/overlay-block', array(
        'editor_script' => 'uni-overlay',
        'editor_style' => 'uni-gutenberg-editor',   
    ) );   
    
    wp_register_script('uni-logo-slider', asset_path('scripts/logo-slider.js'), array( 'wp-blocks', 'wp-element', 'wp-block-editor' ), null );
    register_block_type( 'uni/logo-slider', array(
        'editor_script' => 'uni-logo-slider',
        'editor_style' => 'uni-gutenberg-editor',   
    ) );

    wp_register_script('uni-slider', asset_path('scripts/uni-slider.js'), array( 'wp-blocks', 'wp-element', 'wp-block-editor', 'wp-components' ), null );
    register_block_type( 'uni/uni-slider', array(
        'editor_script' => 'uni-slider',
        'editor_style' => 'uni-gutenberg-editor',   
    ) );

   wp_register_script('uni-collapse-section', asset_path('scripts/collapse-section.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element'), null );
    register_block_type( 'uni/collapse-section', array(
        'editor_script' => 'uni-collapse-section',  
        'editor_style' => 'uni-gutenberg-editor',
    ) );

    wp_register_script('uni-collapse-list', asset_path('scripts/collapse-list.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element', 'wp-components'), null );
    register_block_type( 'uni/collapse-list', array(
        'editor_script' => 'uni-collapse-list',  
        'editor_style' => 'uni-gutenberg-editor',
    ) );

    wp_register_script('uni-collapse-block', asset_path('scripts/collapse-block.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element'), null );
    register_block_type( 'uni/collapse-block', array(
        'editor_script' => 'uni-collapse-block',  
        'editor_style' => 'uni-gutenberg-editor',
    ) );

    wp_register_script('uni-info-table', asset_path('scripts/info-table.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element'), null );
    register_block_type( 'uni/info-table', array(
        'editor_script' => 'uni-info-table',  
        'editor_style' => 'uni-gutenberg-editor',
    ) );

    wp_register_script('uni-info-list', asset_path('scripts/info-list.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element', 'wp-components'), null );
    register_block_type( 'uni/info-list', array(
        'editor_script' => 'uni-info-list',  
        'editor_style' => 'uni-gutenberg-editor',
    ) ); 

   wp_register_script('uni-js-counter', asset_path('scripts/js-counter.js'), array( 'wp-blocks', 'wp-block-editor' , 'wp-element'), null );
    register_block_type( 'uni/js-counter', array(
        'editor_script' => 'uni-js-counter',  
        'editor_style' => 'uni-gutenberg-editor',
    ) );
});


/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s col">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'       
    ]);
    register_sidebar([
        'name'          => __('Quick Access', 'sage'),
        'id'            => 'sidebar-quick',
        'before_widget' => '<div class="qs-wdgt %1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''       
    ]);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});
