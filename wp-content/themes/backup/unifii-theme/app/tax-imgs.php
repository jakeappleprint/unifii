<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


function uni48_cat_save($term_id) {
    if(isset($_POST['uni-cat-image-id'])) {
        update_term_meta($term_id,'_cat_image_id',$_POST['uni-cat-image-id']);
	}  
}
add_action('edited_category','uni48_cat_save');
add_action('category_edit_form_fields', 'uni_cat_img_metabox',20,1);


function uni_cat_img_metabox($cat) {
	wp_enqueue_media();
 	$title = 'Category Banner Image';
 	$value = get_term_meta($cat->term_id, '_cat_image_id', true);
 	$upload_link = esc_url( get_upload_iframe_src( 'image', $cat->term_id ) );
	
	if(!empty($value)) {
		$value_src = wp_get_attachment_image_src( $value, 'slide' );
	} else {
		$value_src = false; 
	}	 ?>
	<table id="uni-cat-image" class="form-table"><tbody><tr>
		<th scope="row"><label>
		<?php echo $title; ?>	
		</label></th>
		<td>		

			<?php	// Get the image src
			$img_exists = is_array($value_src); ?>
			<div id="current-uni-cat-image">
			    <?php if ( $img_exists ) : ?>
			        <img src="<?php echo $value_src[0] ?>" alt="" style="max-width:100%;" />
			    <?php endif; ?>
			</div>
			<!-- Your add & remove image links -->
			<p class="hide-if-no-js">
			    <a class="upload-uni-cat-image <?php if ( $img_exists  ) { echo 'hidden'; } ?>" 
			       href="<?php echo $upload_link ?>">
			        <?php _e('Choose Image') ?>
			    </a>
			    <a class="delete-uni-cat-image <?php if ( ! $img_exists  ) { echo 'hidden'; } ?>" 
			      href="#">
			        <?php _e('Remove this image') ?>
			    </a>
			</p>			
			<input id="uni-cat-image-id" name="uni-cat-image-id" type="hidden" value="<?php echo esc_attr( $value ); ?>"/>
			<script>
			jQuery(function($){

		  // Set all variables to be used in scope
		  var frame,
		      metaBox = $('#uni-cat-image'), // Your meta box id here
		      addImgLink = metaBox.find('.upload-uni-cat-image '),
		      delImgLink = metaBox.find( '.delete-uni-cat-image '),
		      imgContainer = metaBox.find( '#current-uni-cat-image'),
		      imgIdInput = metaBox.find( '#uni-cat-image-id' );
		  
		  // ADD IMAGE LINK
		  addImgLink.on( 'click', function( event ){    
		    event.preventDefault();
		    
		    // If the media frame already exists, reopen it.
		    if ( frame ) {
		      frame.open();
		      return;
		    }
		    
		    // Create a new media frame
		    frame = wp.media({
		      title: 'Select or Upload an Image',
		      button: {
		        text: 'Use this media'
		      },
		      multiple: false  // Set to true to allow multiple files to be selected
		    });

		    
		    // When an image is selected in the media frame...
		    frame.on( 'select', function() {      
		      // Get media attachment details from the frame state
		      var attachment = frame.state().get('selection').first().toJSON();
		      // Send the attachment URL to our custom image input field.
		      imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );
		      // Send the attachment id to our hidden input
		      imgIdInput.val( attachment.id );
		      // Hide the add image link
		      addImgLink.addClass( 'hidden' );
		      // Unhide the remove image link
		      delImgLink.removeClass( 'hidden' );
		    });

		    // Finally, open the modal on click
		    frame.open();
		  });
		  
		  
		  // DELETE IMAGE LINK
		  delImgLink.on( 'click', function( event ){
		    event.preventDefault();
		    // Clear out the preview image
		    imgContainer.html( '' );
		    // Un-hide the add image link
		    addImgLink.removeClass( 'hidden' );
		    // Hide the delete image link
		    delImgLink.addClass( 'hidden' );
		    // Delete the image id from the hidden input
		    imgIdInput.val( '' );
		  });

		});
			</script>


		</td></tr></tbody></table>
	<?php	
}
