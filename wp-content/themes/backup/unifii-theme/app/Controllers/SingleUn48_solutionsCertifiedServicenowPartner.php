<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleUn48_solutionsCertifiedServicenowPartner extends Controller
{
    public function servicenowSlider(){
        # Live: [get_the_ID(), 207, 26];
        # Dev: [get_the_ID(), 299, 26];
        $items = [get_the_ID(), 207, 26];
        $slides = array();
        $opts = get_option('un48_theme_opts');
        foreach($items as $i) {
            $title = get_the_title($i);
            $link = get_permalink($i);
            $img = wp_get_attachment_image_src(get_post_thumbnail_id($i), 'slide');
            if(get_the_ID() === $i && $opts['company_logo']) {
                $link = '#content';
                $company_logo =  wp_get_attachment_image($opts['company_logo'], 'full-size', false, ['class' => 'company-logo', 'alt' => esc_attr(get_bloginfo('title'))]);
                $logo = get_post_meta($i, '_un48_logo_id', true);
                $logo = wp_get_attachment_image($logo, 'full-size', false, ['class' => 'slide-logo', 'alt' => esc_attr($title)]);
                $headline = get_post_meta($i, '_un48_banner_txt', true);
                $title = '<div class="with-image two-logos"><h1 class="sr-only sr-focusable">'. $title .'</h1>'. $company_logo .'<span></span>'. $logo .'</div>';
                $title.= '<p class="headline">'. $headline .'</p>';
            } else if(26 === $i) {
                $title = '<div class="servicenow-logo">'.$logo.'<h2>' . $title . '</h2></div>';
                $headline = get_post_meta($i, '_un48_navtxt', true);
                $title.= '<p class="headline">'. $headline .'</p>';
            } else {
                $title = '<div class="servicenow-logo">'.$logo.'<h2>' . $title . '</h2></div>';
            }
            $slides[] = [
                'title' => $title,
                'url' => $link,
                'image' => '<div class="bg-image" style="background-image: url('. $img[0] .');"></div>',
            ];
        }
        if(!empty($slides)) {
            return $slides;
        }
    }
}
