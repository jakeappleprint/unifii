@if(isset($the_services))
<section id="quick-services" class="uni-services">
    <div class="wrap">
        <div class="container">   
            <h3 class="title">Our Services</h3>
            <p class="subtitle">Re-think business processes, automate workflows and transform the customer and employee experience with Unifii.</p>
            <div class="row">    
                @foreach($the_services as $service)
                    <div class="col-md-6 col-lg-4 service-block row-{{ $loop->iteration%2 }}">
                        <div class="service-wrap">                      
                            <div class="image">
                                {!! $service['image'] !!}
                            </div>
                            <h3>{!! $service['title'] !!}</h3>
                            <div class="entry">{!! $service['text'] !!}</div>
                            <div class="link"><a href="{!! $service['link'] !!}" class="button">Find out more</a></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
