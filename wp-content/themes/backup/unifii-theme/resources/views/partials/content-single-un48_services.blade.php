<article @php post_class('service entry-content') @endphp>
    @php the_content() @endphp
</article>
