@if(!have_posts())
<div class="no-posts wrap">
  <div class="container">
    <div class="alert alert-warning">
      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  </div>
</div>
@endif