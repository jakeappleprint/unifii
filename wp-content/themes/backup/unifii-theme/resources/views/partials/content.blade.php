<article @php post_class('unifii-index col-md-6 col-lg-4') @endphp>
  <div class="inner">
    <div class="image">@php the_post_thumbnail('post-thumb') @endphp</div>
    <div class="entry">
      <h3>{!! get_the_title() !!}</h3>
      <div class="entry-summary">
        @php echo get_the_excerpt() @endphp
      </div>
      <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
    </div>
  <a href="{{ get_permalink() }}"><div class="overlay">
      <div class="centred"> 
        <div class="centre"><span class="button alt">Read more</span></div>
      </div>
  </div></a>
</article>
