<div class="col-md-6 col-lg-3 service-block">
  <div class="service-wrap">
    <div class="image">
      @php $image = get_post_meta(get_the_ID(), '_un48_slogo_id', true); @endphp
      @if(!empty($image))
        {!! wp_get_attachment_image($image, 'square', false, ['class' => 'uni-service-image' ]) !!} 
      @endif
    </div>
    <h4><strong>{!! get_the_title() !!}</strong></h4>
      <div class="entry">
        @php echo get_the_excerpt() @endphp
      </div> 
      <div class="link"><a href="{{ get_permalink() }}" class="button">Find out more</a></div>
  </div>
  <p>
</div>
