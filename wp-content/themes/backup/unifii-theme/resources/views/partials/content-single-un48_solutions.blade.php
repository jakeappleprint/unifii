<article id="content" @php post_class('solution entry-content') @endphp>
    @php the_content() @endphp
</article>
