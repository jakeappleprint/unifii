
<div class="row entry-meta no-gutters">
  <div class="col icon-col">
    @if(isset($the_gravatar))
      {!! $the_gravatar !!}
    @endif
  </div>
  <div class="col desc-col">
    <p>Written By:  <strong>{{ get_the_author() }}</strong></p>
    <p>{{ $the_job_desc }}</p>
  </div>
</div>
