<form role="search" method="get" class="search-form" action="{!! esc_url(home_url()) !!}">
	<label>
		<span class="sr-only sr-only-focusable">Search for:</span>
		<input type="search" class="search-field" placeholder="Search …" value="@php the_search_query() @endphp" name="s">
	</label><span class="submit-icon"><input type="submit" class="search-submit" value="Search"></span>	
</form>
