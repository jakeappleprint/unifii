@if(isset($latest_posts))
@php $c = count($latest_posts)  @endphp
	<section id="unifii-latest-news" class="uni-latest-news">
		<div class="wrap">
			<div class="container">
				<h3 class="title">{{ $latest_posts['title']  }}</h3>
				<div class="latest-news-slider">
					<div id="latest-news-frame" class="frame">
						<div id="latest-news-rail" class="rail row no-gutters">
							@foreach($latest_posts['posts'] as $item)
								<div class="news-item col">
									<div class="inner">
										<div class="image">{!! $item['img'] !!}</div>
										<div class="news-content">
											<h5>{{ $item['ttl'] }}</h5>
											<a href="{!! $item['lnk'] !!}" title="Read More">Read More</a>
										</div>	
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<div id="latest-news-left" class="uni-nav uni-left-nav"><span><i class="fa fa-chevron-left"></i></span></div>
					<div id="latest-news-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>
				</div>
			</div>
		</div>
	</section>
@endif
