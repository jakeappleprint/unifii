@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  <div class="container">
    <div class="wrap">
      <h2 class="title">The page you were looking for was not found!</h2>
    </div>
  </div>

  @include('partials.no-posts')
@endsection

@section('after-main-content')
  @include('partials.latest-posts')
@endsection