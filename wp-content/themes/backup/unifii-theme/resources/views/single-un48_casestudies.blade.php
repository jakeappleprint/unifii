@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
      <article @php post_class('case-study wrap entry-content') @endphp>
        <div class="container">
          <h1 class="title">@php the_title() @endphp</h1>
          @php the_content() @endphp
        </div>
      </article>
  @endwhile
@endsection

@section('after-main-content')
  @include('partials.case-studies')
  @include('partials.latest-posts')
@endsection