(function(blocks, element, blockEditor, components ){
  let el = element.createElement 
  const { InnerBlocks, InspectorControls } = blockEditor
  const { PanelBody, PanelRow, TextControl } = components

  const LS_TEMPLATE = [
    ['core/image', {}],
    ['core/image', {}],
    ['core/image', {}],
  ]


  blocks.registerBlockType('uni/logo-slider', {
      title: 'Unifii Logo Slider',
      icon: 'awards',
      category: 'common', 
      attributes: {
       sliderTitle: {
         type: 'string',
         default: 'Our valued clients include:',
       },
     },

      edit: function(props){

        const updateSTitle = function(value){
          props.setAttributes({ sliderTitle: value })
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'Block Title'},
              el(PanelRow, {},
                el(TextControl, {
                  value: props.attributes.sliderTitle,
                  onChange: updateSTitle,
                })
              )
            )
          ),
          el( 'div', {},
            el( InnerBlocks, {              
                template: LS_TEMPLATE,
                allowedBlocks: ['core/image'],
            })
          ),  
        ]
    },
    save: function(props){
        return el( 'div', { id: 'uni-logo-slider' },
          el('div', { className: 'wrap' },
            el('div', { className: 'container'},
              el('div', { className: 'row' },
                el('div', { className: 'col logo-description' },
                  el('p', {}, props.attributes.sliderTitle)
                ),
                el('div', { className: 'col' },
                  el('div', { 
                    id: 'logo-slider-frame',
                    className: 'logo-slider frame',
                  },
                  el('div', {
                      id: 'uni-ls-rail',
                      className: 'rail logo-slider-rail',
                    },
                    el(InnerBlocks.Content, {})
                    )
                  )
                )
              )
            )
          )
        )
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
  window.wp.components
))
