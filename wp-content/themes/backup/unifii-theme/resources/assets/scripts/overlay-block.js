(function(blocks, element, blockEditor, components ){
  let el = element.createElement 
  const { InnerBlocks, MediaUploadCheck, MediaUpload, InspectorControls } = blockEditor
  const { PanelBody, PanelRow, Button } = components

  const OB_TEMPLATE = [
    ['core/group', {}, [
      ['core/heading', { level: 3}],
      ['core/paragraph', {}],
    ]],
  ]


  blocks.registerBlockType('uni/overlay-block', {
      title: 'Unifii Banner Block',
      icon: 'feedback',
      category: 'common', 
      attributes: {
       backgroundCss: {
         type: 'string',
         default: null,
       },
     },

      edit: function(props){

        const updateBGImage = function(value){
          props.setAttributes({ backgroundCss: value.sizes.full.url })
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'Background Image'},
              el(PanelRow, {},
                el(MediaUploadCheck, {}, 
                  el(MediaUpload, {
                    multiple: false,
                    type: 'image',
                    value: props.attributes.backgroundID,
                    onSelect: updateBGImage,
                    render: (open) => {
                      return el(Button, { 
                        onClick: open.open,
                        className: 'is-default is-large is-button',
                      }, 'Choose Background Image')
                    },
                  })
                )
              )
            )
          ),
          el( 'div', { 
              className: props.className,
              style: {
                backgroundImage: 'url('+ props.attributes.backgroundCss +')',
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                color: '#fff',
              },
            },
            el( InnerBlocks, {              
                template: OB_TEMPLATE,
                templateLock: false,
            })
          ),  
        ]
    },
    save: function(props){
        let cN = props.className + ' overlay-banner blur-image'
        return el( 'div', {
            className: cN,

          },
          el('div', { className: 'wrap' },
            el('div', {
              className: 'background-image',
              style: {
                backgroundImage: 'url('+ props.attributes.backgroundCss +')',
              },
            }),
            el(InnerBlocks.Content, {})
          )
        )
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
  window.wp.components
))
