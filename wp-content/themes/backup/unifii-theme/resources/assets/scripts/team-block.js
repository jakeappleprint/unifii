(function(blocks, blockEditor, element){
  let el = element.createElement
  const { InnerBlocks } = blockEditor

  const TEAMS_TEMPLATE = [   
    ['core/image', {
      size: 'square',
      caption: false, 
    } ],
    ['core/heading', {
      level: 4,
      className: 'title',
      placeholder: 'Name',
    }],  
    ['core/paragraph', {
      className: 'job-title',
      placeholder: 'Job Title',
    }], 
  ]

  blocks.registerBlockType('go/team-block', {
      title: 'GO Team Block',
      icon: 'businessman',
      category: 'common',
      parent: 'go/team-members',
      edit: function(props){
        return el( 'div', { className: props.className },
          el( InnerBlocks, {              
              template: TEAMS_TEMPLATE,
              templateLock: true,
          })
        )
    },
    save: function(){
      
      return el('div', { className: 'team-col' }, 
        el('div', { className: 'inner' }, 
          el(InnerBlocks.Content, {})
        )
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element 
))
