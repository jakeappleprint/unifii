export default {
  init() {
    // JavaScript to be fired on the home page

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    let goCsSlider
    const csSlider = function(d){
      let container =  $('#unifii-case-studies')
      if(container.hasClass('sliding')) {
        return
      }
      container.addClass('sliding')
      let r = $('#case-study-rail')
      const ns = {
        item: $('#case-studies-frame').width(),
        items: r.children().length,
        p: parseInt(r.css('left')),
        d: d,
      }
      ns.np = ns.p + (d * ns.item)

        if(d > 0 && ns.np > 0 ) {
          r.children().first().before(r.children().last())
          r.css('left', ns.item * -1)
          ns.np = 0
        }

      r.animate({'left': ns.np}, 1250, 'easeInOutQuart', function(){
        $('#unifii-case-studies').removeClass('sliding')
        if(1 > ns.d) {
          r.children().last().after(r.children().first())
          r.css('left', 0)
          if(!goCsSlider){
            goCsSlider = setInterval(function(){
              csSlider(1)
            }, 7250);
          }
        }
      })
    }

    goCsSlider = setInterval(function(){
      csSlider(1)
    }, 7250);

    $('#unifii-case-studies .uni-nav').click(function(){
      let d = -1
      if($(this).hasClass('uni-left-nav')) {
        d = 1
      }
      clearInterval(goCsSlider);
      csSlider(d)
    })
  
  },
};
