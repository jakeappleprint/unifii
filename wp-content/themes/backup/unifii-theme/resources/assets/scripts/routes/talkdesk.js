export default {
  init() {
    const fixRvwWidths = function(){
      const r = $('#review-rail')
      const dz = {
        qty: r.children('.row').children().length,
        w: 353,
      }
      if(750 > window.innerWidth) {
        dz.w = 253
      }
      dz.containerWidth = dz.qty * dz.w
      r.css('width', dz.containerWidth)   
    }
    fixRvwWidths
    window.onload = fixRvwWidths
    window.resize = fixRvwWidths

    if($('#talkdesk-video')) {
      $('.watch-video-link').append($('#talkdesk-video'))
    }

  },
  finalize() {
    console.log('yoooo')
    $('.download-brochure').children().children('a').click(function(e){
      e.preventDefault()
      let p = $(this).parent().parent('.download-brochure')
      if(p.hasClass('form-open')) {
        $(this).text('Download Brochure')
      } else {
        $(this).text('Close Form')
      }

      p.toggleClass('form-open')
      p.next('.wpcf7').slideToggle()
    })

    document.addEventListener('wpcf7mailsent', function(ev){
      if(541 === parseInt(ev.detail.contactFormId)) {
        $('.download-brochure').fadeOut()
        $('#brochure-form').fadeOut(function(){
          $(this).remove()
          let id = '#' + ev.detail.id,
          url = 'http://unifii.dev-preview.website/wp-content/uploads/2020/05/talkdesk_brochure_2019_10.pdf'
          $(id).after($('<div class="wp-block-button"><a href="' + url + '" class="wp-block-button__link" download>Download</a></div>'))          
        })
      }
      
    }, false)
  
    const shiftRvws = function(d){
      let container =  $('#uni-rvw-frame')
      if(container.hasClass('sliding')) {
        return
      }
      container.addClass('sliding')
      const rail = $('#review-rail')
      const ls = {
        kids: rail.children('.row').children(),
        width: rail.children('.row').children().first().outerWidth() * -1,
      }
      if(d === 1) {
        ls.kids.first().before(ls.kids.last())
        rail.css('left', ls.width)
        rail.animate({'left': 0}, 1250, 'easeInOutQuint', function(){
          container.removeClass('sliding')
          ls.status = true
        })
      } else {
        rail.animate({'left': ls.width}, 1250, 'easeInOutQuint', function(){
          ls.kids.last().after(ls.kids.first())
          rail.css('left', 0)  
          container.removeClass('sliding')
          ls.status = true     
        })
      }

      if(ls.status) {
        rvwSlider  = setInterval(function(){
            shiftRvws(1)
        }, 6600)
      }
    }
    
    let rvwSlider
    rvwSlider  = setInterval(function(){
        shiftRvws(1)
    }, 6600)

    $('#nvslr-carousel .uni-nav').click(function(){
      clearInterval(rvwSlider)
      let d = -1
      if($(this).hasClass('uni-left-nav')) {
        d = 1
      }
      shiftRvws(d)
    })
  
  },
};
