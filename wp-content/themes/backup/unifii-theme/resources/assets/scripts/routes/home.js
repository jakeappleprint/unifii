export default {
  init() {
    // JavaScript to be fired on the home page
    const backgroundHeightResize = function(){
      let container = $('#slider .bg-image')
      if(768 > window.innerWidth) {
        
        const h = {
          hgt: 0,
          hgts: [
            window.innerHeight,
            screen.height,
            $(window).height(),
          ],
          header: $('#masthead').height(),
        }

        for (let index = 0; index < h.hgts.length; index++) {
          if(h.hgts[index] !== undefined) {
            h.hgt = h.hgts[index] - h.header
            container.css('height', h.hgt)
            break
          }
        }
      } else { 
        container.css('height', '')
       }
    }


    const swapSrvcTtls = function(){
      $('#quick-services .service-block').each(function(){
        if(767 > window.innerWidth && !$(this).hasClass('swapped')) {
          $(this).addClass('swapped')
          let ttl = $(this).find('.title')
          $(this).find('.row').prepend(ttl)
        } else if(window.innerWidth > 767  && $(this).hasClass('swapped')){
          let ttl = $(this).find('.title')
          $(this).find('.entry').prepend(ttl)
          $(this).removeClass('swapped')
        } else {
          return
        }
      })
    }

    const homeFuncs = function(){
      backgroundHeightResize()
      swapSrvcTtls()
    }

    window.onload = homeFuncs
    window.onresize = homeFuncs 
    
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

  },
};
