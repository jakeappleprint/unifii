import 'jquery-appear-original';
export default {
  init() {

    $(function(){
      $('html').hide();
    });
    $(window).load(function() {
      $('html').fadeIn();
    });

    // JavaScript to be fired on all page
    if($('#unifii-latest-news').length !== 0) {
      let x = $('#latest-news-rail').children().length
      x = x * 373
      $('#latest-news-rail').css('width', x)
    }

    $('#mainMenu .menu-item-has-children').each(function(){
      $(this).children('.sub-menu').after($(this).children('.submenu-toggle'))
    })

    $('.widget .menu-item-has-children').each(function(){
      $(this).children('a').append($(this).children('.submenu-toggle'))
    })

    if($('.create-nav').length > 0) {
      let cNav = $('.create-nav')
      cNav.each(function(){
        let topNav = $(this)
        let titles = topNav.parent('div').find('.grab-title')
        titles.each(function(i){
          if(i === 0) {
            $(this).parent().addClass('collapse-open')
          }
          topNav.append($('<li></li>').addClass('nav-item').append($(this)))
        })
        topNav.children().first().addClass('active')
      })

      $('.collapse-list-title').each(function(e){
        let t = $(this).text()
        $('.wp-block-uni-collapse-list').eq(e).attr('id', t);
      })
    }

    if($('.wp-block-uni-info-table').length > 0) {
      $('.wp-block-uni-info-list').each(function(n){
        $(this).children('ul').addClass('no-styles')
        let css = { display: 'block' }
        if(n === 0){
          $(this).addClass('activated')
          $(this).children('ul').css(css)
        }
      })
    }

    if($('.wp-block-uni-logo-slider').length > 0) {
      const logoSliderItems = $('#uni-ls-rail').children()
      let sl = {
        items: logoSliderItems.length,
        width: 210,
      }
      sl.railWidth = sl.items * sl.width
      $('#uni-ls-rail').css('width', sl.railWidth)
      for (let i = 0; i < sl.items; i++) {
        logoSliderItems.eq(i).wrap('<div class="logo-slider-item"><div class="centred"></div></div>')
      }
      $('#uni-logo-slider .logo-description').children().wrapAll('<div class="centred"><div class="centre"></div></div>')
      $('#uni-logo-slider .logo-description').next().append($('<div id="logo-slider-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>'))
    }

    if($('.wp-block-uni-uni-slider').length > 0) {
      let l = $('#uni-slider-rail').children().length * 100
      l+='%'
      $('#uni-slider-rail').css('width',l)
    }

    if(0 < $('.wp-block-gallery').length) {
      $('.wp-block-gallery ul').each(function(){
        $(this).children('li').children('figure').children().wrap($('<div class="centred"><div class="centre"></div></div>'))
      })
    }

    if(0 < $('.button-icon').length) {
      $('.button-icon').each(function(){
        $(this).before($('<button href="#'+ $(this).attr('id') +'" class="quick-button"><i class="fa fa-chevron-down"></i></button>'))
        console.log($(this))
      })
    }

    if($('.wp-block-uni-overlay-block').length > 0) {
      $('.wp-block-uni-overlay-block').each(function(){

        $(this).find('h2').addClass('title')
        $(this).find('h3').each(function(){
          if(!$(this).parents('.wp-block-columns')) {
            $(this).addClass('title')
          }
        })
      })
    }
  },
  finalize() {

    $('.post-type-archive-un48_solutions .headline').text('Solutions');
    // JavaScript to be fired on all pages, after page specific JS is fired
    const bodyScrollLock = require('body-scroll-lock');
    const disableBodyScroll = bodyScrollLock.disableBodyScroll;
    const enableBodyScroll = bodyScrollLock.enableBodyScroll;

    const menuToggler = function(){
     // let w = $(window).outerWidth()
     let menuBox = document.querySelector('#mainMenu')
     if(975 > screen.width && $('body').hasClass('menu-active')) {
      enableBodyScroll(menuBox)
      $('#mainMenu').animate({'bottom': '-100%'}, 500, function(){
        $(this).attr('style', '')
        $('body').removeClass('menu-active')
      })
    } else if(975 > screen.width) {
      disableBodyScroll(menuBox)
      $('body').addClass('menu-active')
      $('#mainMenu').animate({
        'bottom': 0,
      }, 500)
    }
  }

  $('.nav-toggle').click(menuToggler)
    //$('#mainMenu').click(menuToggler)
    $('.menu-active .menu-cover').click(menuToggler)

    $('.submenu-toggle').click(function(){
      $(this).children('i').toggleClass('fa-chevron-down fa-chevron-up')
      if($(this).parent('li').hasClass('sub-menu-open')) {
        $(this).parent('li').removeClass('sub-menu-open')
        $(this).siblings('.sub-menu').slideUp(function(){
          $(this).attr('style', '')
        })
      } else {
        $(this).parent('li').addClass('sub-menu-open')
        $(this).siblings('.sub-menu').slideDown()
      }
    })

    $('.site-header .menu-item').click(function(){
      if(!$(this).hasClass('menu-item-has-children')) {
        menuToggler()
      }
    })

/*    $('#main-menu').children('.menu-item-has-children').children('a').click(function(e){
      e.preventDefault()
      $(this).parent('li').toggleClass('sub-menu-open')
      if(750 > screen.width) {
        $(this).siblings('.sub-menu,.mm-panel').slideToggle()
      }
    })
    */
    $('.site-footer .menu-item-has-children').children('a').click(function(e){
      e.preventDefault()
      $(this).parent('li').toggleClass('sub-menu-open')
      $(this).siblings('.sub-menu,.mm-panel').slideToggle()
    })

    const quickSidebarToggle = function(){
      $('#quick-wrap').slideToggle()
    }
    $('#quick-sidebar-access').click(quickSidebarToggle)
    $('#close-sidebar').click(quickSidebarToggle)


    $('a[href*=\\#]:not([href=\\#])').click(function() {
      if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
        var target = $(this.hash)
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          let t = target.offset().top - parseInt(target.css('margin-top'));
          $('html,body').animate({
            scrollTop: t,
          }, 750);
          return false;
        }
      }
    })

    let imageSlider
    if($('.wp-block-uni-uni-slider').length > 0) {
      const shiftSlides = function(){
        let rail = $('#uni-slider-rail')
        let sl = {
          width: $('#uni-slider').width() * -1,
        }
        rail.css('left', sl.width)
        rail.children().first().before(rail.children().last())
        rail.animate({'left': 0}, 2000, 'easeInOutQuad', function(){
          $('.uni-nav').removeClass('sliding')
        })
      }
      const shiftSlidesOtherWay = function() {
        let rail = $('#uni-slider-rail')
        let sl = {
          width: $('#uni-slider').width() * -1,
        }
        rail.animate({'left': sl.width}, 2000, 'easeInOutQuad', function(){
          rail.css('left', 0)
          rail.children().last().after(rail.children().first())
          $('.uni-nav').removeClass('sliding')
        })
      }
      imageSlider = setInterval(function(){
        shiftSlides()
      }, 6600)

      $('#uni-slider .uni-nav').click(function(){
        if(!$('.uni-nav').hasClass('sliding')) {
          $(this).addClass('sliding')
          clearInterval(imageSlider)
          if($(this).hasClass('uni-left-nav')) {
            shiftSlides()
          } else {
            shiftSlidesOtherWay()
          }
        } else return

      })
    }


    if($('.wp-block-uni-logo-slider').length > 0) {
      const shiftLogos = function(t, e){
        let rail = $('#uni-ls-rail')
        if(rail.hasClass('logos-moving')) {
          return
        }
        rail.toggleClass('logos-moving')
        let ls = {
          width: -210,
        }
        rail.css('left', ls.width)
        rail.children().first().before(rail.children().last())
        rail.animate({'left': 0}, t, e, function(){
          rail.toggleClass('logos-moving')
        })
      }
      setInterval(function(){
        shiftLogos(1950, 'easeInOutQuad')
      }, 6600)

      $('#uni-logo-slider .uni-nav').click(function(){
        shiftLogos(499, 'easeOutQuad')
      })
    }

    if($('#unifii-latest-news').length !== 0) {
      const newsSlider = function(d){
        $('#unifii-latest-news').addClass('sliding')
        let r = $('#latest-news-rail')
        const ns = {
          item: 373,
          items: r.children().length,
          p: parseInt(r.css('left')),
          d: d,
        }
        ns.max = ($('#latest-news-frame').width() - ns.item)*-1
        ns.np = ns.p + (d * ns.item)

          // go left
          if(d > 0 && ns.np > 0 ) {
            r.children().first().before(r.children().last())
            r.css('left', ns.item * -1)
            ns.np = 0
          }

          r.animate({'left': ns.np}, 750, 'easeOutCubic', function(){
            $('#unifii-latest-news').removeClass('sliding')
            if(1 > ns.d) {
              r.children().last().after(r.children().first())
              r.css('left', 0)
            }
          })
        }

        $('#unifii-latest-news .uni-nav').click(function(){
          let d = -1
          if($(this).hasClass('uni-left-nav')) {
            d = 1
          }
          newsSlider(d)
        })
      }
      if($('#collapseNav').length > 0) {
        $('#collapseNav h5').click(function(){
          let li = $(this).parent('li')
          if(!li.hasClass('active')) {
            let collapseID = '#' + $(this).text()
            $('.collapse-open').slideUp(375, 'easeOutCubic', function(){
              $(this).removeClass('collapse-open')
              $(collapseID).slideDown(500, 'easeInCubic', function(){
                $(this).addClass('collapse-open')
                $('#collapseNav li').removeClass('active')
                li.addClass('active')
              })
            })
          }
        })

        $('.wp-block-uni-collapse-block .card-header').click(function(){
          $(this).next().slideToggle(800, 'easeInOutSine')
          $(this).toggleClass('collapse-block-open')
        })
      }

      if($('.wp-block-uni-info-table').length > 0) {
        $('.wp-block-uni-info-list h5').click(function(){
          console.log(window.innerWidth )
          if(767 >= window.innerWidth ) {
            $(this).next().slideToggle(555, 'easeOutExpo')
            $(this).parent().toggleClass('activated')
            let posi = $(this).offset()
            if(!$(this).parent().hasClass('activated')) {
              posi = $(this).parents('.wp-block-uni-info-table').offset()
            }
            $('html,body').animate({
              scrollTop: posi.top,
            }, 200, 'easeOutQuint')
          } else {
            if(!$(this).parent('.wp-block-uni-info-list').hasClass('activated')) {
              let wrap = $(this).parents('.wp-block-uni-info-table')
              let fHgt = wrap.outerHeight();
              let openThis = $(this).next()
              wrap.css('min-height', fHgt)

              $('.wp-block-uni-info-list.activated ul').slideUp(375, 'easeOutExpo', function(){
                openThis.slideDown(750, 'easeOutQuint', function(){
                  wrap.attr('style', '')
                })
              })
              wrap.find('.wp-block-uni-info-list.activated').removeClass('activated')
              openThis.parent().addClass('activated')
            }
          }
        })
      }

      if(0 < $('.button-icon').length) {
        $('.main').on('click', '.quick-button', function(){
          let target = $(this).attr('href')
          target = $(target)
          if (target.length) {
            let t = target.offset().top
            $('html,body').animate({
              scrollTop: t,
            }, 750);
            return false;
          }
        })
      }


      const raiseItUp = function(block){
        let d = {
          p: block,
          e: block.children('span'),
          dur: 1250,
          num: 0,
          max: block.children('span').data('max'),
        }
        d.oMax = d.e.data('duration-max')
        d.steps = d.dur / d.oMax;
        d.thisDuration = d.steps * d.max
        $({countNum: d.num}).animate({countNum: d.max},{
          duration: d.thisDuration,
          easing: 'linear',
          step: function(){
            d.e.html(Math.floor(this.countNum))
          },
          complete: function(){
            d.e.html(this.countNum)
            d.p.addClass('finished')
          },
        })
      }

      if(0 !== $('.wp-block-uni-js-counter').length) {
        let lH = 0
        $('.wp-block-uni-js-counter').each(function(){
          let e = $(this).children('span')
          let val = e.html()
          e.attr('data-max', val)
          if(val > lH) {
            lH = val
          }
          e.html(0)
        })
        $('.wp-block-uni-js-counter span').attr('data-duration-max', lH)
        $('.counter-block-wrapper').each(function(i){
          $(this).attr('id', 'js-counter-block-' + i)
        })

        for (let index = 0; index < $('.counter-block-wrapper').length; index++) {
          let blockID = '#js-counter-block-' + index
          $(blockID).appear()
          $(blockID).on('appear', function(){
            $(this).find('.wp-block-uni-js-counter').each(function(){
              if(!$(this).hasClass('finished')) {
                raiseItUp($(this))
              }
            })
          })
        }
      }
    },
  };
