export default {
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

    let goSlider;
    const sliderMove = function(d,t,e) {
      $('#slider-frame').addClass('in-progress')
      let frame = $('.slider').find('.frame')
      let rail = $('#slider')
      const config = {
        width: frame.width(),
        actual: frame.width()*-1,
        slides: rail.children('.slide').length,
      }

      if(d===-1) {
        rail.children('.slide').first().before(rail.children('.slide').last())
        rail.css('left', config.actual)
        config.finishPos = 0
      } else {
        config.finishPos = config.actual
      }
      rail.animate({'left': config.finishPos },t,e,function(){
        if(config.finishPos !== 0) {
          rail.children('.slide').last().after(rail.children('.slide').first())
          rail.css('left', 0)
        }
        $('#slider-frame').removeClass('in-progress')
        if(!goSlider) {
          goSlider = setInterval(function(){
            sliderMove(1,1850,'easeInOutQuart');
          }, 6000);
        }
      })
    }

    $('#slider-frame .uni-nav').click(function(){
      let d = 1
      if($(this).hasClass('uni-left-nav')) {
        d = -1
      }
      clearInterval(goSlider);
      sliderMove(d, 1000,'easeOutCirc');
    })

    goSlider = setInterval(function(){
      sliderMove(1,1850,'easeInQuart');
    }, 6000);
  },
};
