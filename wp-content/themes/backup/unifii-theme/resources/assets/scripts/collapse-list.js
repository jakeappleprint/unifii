(function(blocks, blockEditor, element, components ){
  let el = element.createElement 
  const { InnerBlocks, InspectorControls } = blockEditor
  const { PanelBody, PanelRow, TextControl } = components

  const CL_TEMPLATE = [ 
    ['uni/collapse-block', {}],
    ['uni/collapse-block', {}],
  ]

  blocks.registerBlockType('uni/collapse-list', {
      title: 'Unifii Collapse List',
      icon: 'list-view',
      category: 'common',
      supports: { 
        inserter: false,
      },
      attributes: {
        collapseTitle: {
          type: 'string',
          default: null,
        },
      },
      edit: function(props){
        const updateTitle = function(v){
          props.setAttributes({collapseTitle: v})
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'List Title'},
              el(PanelRow, {},
                el(TextControl, {
                  value: props.attributes.collapseTitle,
                  onChange: updateTitle,
                })
              )
            )
          ),
          el('div', { className: props.className },
            el('h5', { className: 'collapse-list-title'},  props.attributes.collapseTitle),
            el( InnerBlocks, {              
                template: CL_TEMPLATE,
                allowedBlocks: ['uni/collapse-block'],
            })
          ),
        ]
    },
    save: function(props){
      return el('div', {}, 
          el('h5', { className: 'collapse-list-title grab-title'},  props.attributes.collapseTitle),
          el(InnerBlocks.Content, {})
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element,
  window.wp.components
))
