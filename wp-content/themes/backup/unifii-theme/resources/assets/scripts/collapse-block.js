(function(blocks, blockEditor, element){
  let el = element.createElement
  const { InnerBlocks } = blockEditor

  const  CBL_TEMPLATE = [
    ['core/heading', {
      level: 6,
      className: 'card-header',
      placeholder: 'Collapse Title',
    }],
    ['core/group', {
        className: 'card-body',
       },
       [['core/paragraph', {
        className: 'job-title',
        placeholder: 'content',
      }]],
    ],
  ]

  blocks.registerBlockType('uni/collapse-block', {
      title: 'Unifii Collapse Block',
      icon: 'download',
      category: 'common',
      parent: 'uni/collapse-list',
      supports: { 
        inserter: false,
      },
      edit: function(props){
        return el( 'div', { className: props.className },
          el( InnerBlocks, {              
              template: CBL_TEMPLATE,
              allowedBlocks: ['core/paragraph'],
          })
        )
    },
    save: function(){
      
      return el('div', { className: 'card' }, 
          el(InnerBlocks.Content, {})
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element 
))
