(function(blocks, blockEditor, element, components ){
  let el = element.createElement 
  const { InnerBlocks, InspectorControls } = blockEditor
  const { PanelBody, PanelRow, TextControl } = components

  const IL_TEMPLATE = [ 
    ['core/list', { className: 'no-styles' }],
  ]

  blocks.registerBlockType('uni/info-list', {
      title: 'Unifii info List',
      icon: 'list-view',
      category: 'common',
      supports: { 
        inserter: false,
      },
      attributes: {
        infoTitle: {
          type: 'string',
          default: 'Enter List Title',
        },
      },
      edit: function(props){
        const updateTitle = function(v){
          props.setAttributes({infoTitle: v})
        }
        return [
          el(InspectorControls, {key: 'inspector'},
            el(PanelBody, {title: 'List Title'},
              el(PanelRow, {},
                el(TextControl, {
                  value: props.attributes.infoTitle,
                  onChange: updateTitle,
                })
              )
            )
          ),
          el('div', { className: props.className },
            el('h5', { className: 'info-list-title'},  props.attributes.infoTitle),
            el( InnerBlocks, {              
                template: IL_TEMPLATE,
                templateLock: true,
            })
          ),
        ]
    },
    save: function(props){
      return el('div', {}, 
          el('h5', { className: 'info-list-title'},  props.attributes.infoTitle),
          el(InnerBlocks.Content, {})
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element,
  window.wp.components
))
