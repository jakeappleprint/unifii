(function(blocks, element, blockEditor ){
  let el = element.createElement 
  const { InnerBlocks } = blockEditor

  const SL_TEMPLATE = [
    ['core/image', { sizeSlug: 'image-slider' }],
    ['core/image', { sizeSlug: 'image-slider' }],
    ['core/image', { sizeSlug: 'image-slider' }],
  ]


  blocks.registerBlockType('uni/uni-slider', {
      title: 'Unifii Image Slider',
      icon: 'images-alt2',
      category: 'common', 
      edit: function(){
        return el( 'div', {},
            el( InnerBlocks, {              
                template: SL_TEMPLATE,
                allowedBlocks: ['core/image'],
            })
          )
    },
    save: function(){
        return el( 'div', { id: 'uni-slider' },
          el('div', { id: 'uni-slider-frame', className: 'frame' },
            el('div', { id: 'uni-slider-rail', className: 'rail image-slider-rail'},
              el(InnerBlocks.Content, {})
            )
          ),
          el('div', { 
            id: 'uni-sl-lnav',
            className: 'uni-nav uni-left-nav',
          },
          el('span',{},
            el('i', { className: 'fa fa-chevron-left' })
          )
        ),
        el('div', { 
            id: 'uni-sl-rnav',
            className: 'uni-nav uni-right-nav',
          },
          el('span',{},
            el('i', { className: 'fa fa-chevron-right' })
          )
        )
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
  window.wp.components
))
