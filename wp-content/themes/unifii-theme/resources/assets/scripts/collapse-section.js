(function(blocks, blockEditor, element){
  let el = element.createElement
  const { InnerBlocks } = blockEditor

  const CS_TEMPLATE = [ 
    ['uni/collapse-list', { focus: true }],
    ['uni/collapse-list', {}],
  ]

  blocks.registerBlockType('uni/collapse-section', {
      title: 'Unifii Collapse Section',
      icon: 'editor-justify',
      category: 'common',
      edit: function(props){
        return el( 'div', { className: props.className },
          el( InnerBlocks, {              
              template: CS_TEMPLATE,
              allowedBlocks: ['uni/collapse-list'],
          })
        )
    },
    save: function(){
      return el('div', { className: 'wrap'}, 
        el('div', { className: 'container' },
          el('ul', { 
            id: 'collapseNav',
            className: 'collapse-nav nav nav-fill create-nav',
          }), 
          el(InnerBlocks.Content, {}),
          el('p', { className: 'link-box' }, [ 
            'Can\'t find what you were looking for?',
             el('a', { 
                href:'/contact',
                className: 'button',
              },
              ['Get in touch']
            ),
          ])
        )
      )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element 
))
