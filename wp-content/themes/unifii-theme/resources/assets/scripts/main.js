// import external dependencies
import 'jquery';
import 'jquery-easing';
import './jquery-cycle';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import archive from './routes/index';
import index from './routes/index';
import unifiiCaseStudies from './routes/case-study';
import talkdeskContactCentreSoftware from './routes/talkdesk';
import unifiiSlider from './routes/slider';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  archive,
  index,
  unifiiCaseStudies,
  talkdeskContactCentreSoftware,
  unifiiSlider,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
