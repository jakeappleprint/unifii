(function(blocks, editor, element){
    let el = element.createElement


    blocks.registerBlockType('uni/js-counter', {
        title: 'Count Upwards',
        icon: 'upload',
        category: 'common',
        attributes: {
            content: {
                type: 'string',
                attribute: 'value',
            },
            label: {
                type: 'string',
                attribute: 'value',
            },
        },
        edit: function(props){
            function updateContent(ev) {
                props.setAttributes({ content: ev.target.value })
            }
            function updateLabel(ev) {
                props.setAttributes({ label: ev.target.value })
            }            
            return el(
                'div',
                {},
                el(
                    'input',
                    { 
                        className: props.className,
                        placeholder: '00',
                        onChange: updateContent,
                        value: props.attributes.content,
                        type: 'text',
                    }
                ),
                el(
                    'input',
                    {
                        className: props.className,
                        placeholder: 'Counter Label',
                        onChange: updateLabel,
                        value: props.attributes.label,
                        type: 'text',
                    }
                ),
                el(
                    'div',
                    { className: props.className },
                    el(
                        'span',
                        {},
                        props.attributes.content
                    ),
                    el(
                        'p',
                        { className: 'counter-label' },
                        props.attributes.label
                    )
                )                
            )
        },
        save: function(props){
            return el(
                'div',
                { className: props.className },
                el(
                    'span',
                    {},
                    props.attributes.content
                ),
                el(
                    'p',
                    { className: 'counter-label' },
                    props.attributes.label
                )
            )
        },
    })
}(
    window.wp.blocks,
    window.wp.editor,
    window.wp.element    
))