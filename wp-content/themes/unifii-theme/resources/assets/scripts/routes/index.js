export default {
  init() {
    // JavaScript to be fired on all page
    //let grid = $('#post-grid')
    const loadMorePosts = function(data){
      if($('body').hasClass('search-results')) {
        data.s = $('.search-form .search-field').val() 
      }
      $.post(themescripts.ajaxurl, data, function(r){
        if( r.success === true ) {
          let content = $(r.data.html)
          let target = {
            e: $('#post-grid article').last(),
          } 
          target.h = target.e.height()
          target.o = target.e.offset()
          target.d = target.o.top + target.h
          $('#post-grid').append(content)
          if(target) {
            $('html,body').animate({
              scrollTop: target.d,
            }, 750, 'easeInOutQuart')              
          }

          if($('#post-grid').children().length >= r.data.pages) {
            $('#load-more-posts').fadeOut().after('<p class="button">All posts displayed!</p>')
          }
          return false; 
        }
      });
    }

    $('#load-more-posts').click(function(){
      let data = {
        action: 'load_posts',
        paged: $(this).data('paged'),
        ncheck: $(this).data('ncheck'),
      }
      if($(this).data('cat')) {
        data.cat = parseInt($(this).data('cat'))
      }
      data.paged = parseInt(data.paged) + 1
      $('#load-more-posts').data('paged', data.paged)
      loadMorePosts(data)
    })
  },
};
