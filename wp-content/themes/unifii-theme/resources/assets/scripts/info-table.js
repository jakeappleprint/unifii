(function(blocks, blockEditor, element){
  let el = element.createElement
  const { InnerBlocks } = blockEditor

  const IT_TEMPLATE = [ 
    ['uni/info-list', { focus: true }],
    ['uni/info-list', { focus: true }],
  ]

  blocks.registerBlockType('uni/info-table', {
      title: 'Unifii Responsive Information Table',
      icon: 'editor-table',
      category: 'common',
      edit: function(props){
        return el( 'div', { className: props.className },
          el( InnerBlocks, {              
              template: IT_TEMPLATE,
              allowedBlocks: ['uni/info-list'],
          })
        )
    },
    save: function(){
      return el('div', {}, 

          el('div', { className: 'container' },
            el('div', { className: 'row' },
              el(InnerBlocks.Content, {})
            )
          )
        )
    },
  })
}(
  window.wp.blocks,
  window.wp.blockEditor,
  window.wp.element 
))
