@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-single')
  @endwhile
@endsection

@section('after-main-content')
  @include('partials.latest-posts')
@endsection