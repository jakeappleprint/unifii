@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection

@section('after-main-content')
  @include('partials.contact-form')
  @include('partials.latest-posts')
@endsection