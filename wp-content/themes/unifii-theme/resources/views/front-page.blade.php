@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.slider')
      @include('partials.content-page')
  @endwhile
@endsection

@section('after-main-content')
  @include('partials.services')
  @include('partials.solutions')
  @include('partials.contact-form')
  @include('partials.case-studies')
  @include('partials.articles')
  @include('partials.latest-posts')
@endsection
