@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  @include('partials.no-posts')

  <div id="unifii-case-studies" class="uni-case-studies">
	  <div class="wrap">
		  <div class="container">
			  <div class="cs-wrapper">
			    <div class="case-studies-slider">
				    <div id="case-studies-frame" class="frame">
					    <div id="case-study-rail" class="rail row no-gutters" style="width: 400%;">
							@while(have_posts()) @php the_post() @endphp
							@php $logo = get_post_meta(get_the_ID(), '_un48_logo_id', true) @endphp
							@php $logo = wp_get_attachment_image_src($logo, 'full-size') @endphp
							<div class="un48-cs col">
							    <div class="inner blur-image">
								    <div class="image">@php the_post_thumbnail('case-study', ['class' => 'blur-this']) @endphp</div>
								    <div class="overlay">
									    <div class="centred">
										    <div class="centre">
											    <img src="{!! $logo[0] !!}" class="cs-logo" alt="{{ get_the_title() }}" />
                        					</div>
									    </div>
								    </div>
								    <div class="overlay link-overlay"><a href="{!! the_permalink() !!}" class="alt button">Find out more</a></div>
							    </div>
						    </div> 
							@endwhile
						</div>
				    </div>
				    <div id="case-study-left" class="uni-nav uni-left-nav"><span><i class="fa fa-chevron-left"></i></span></div>
				    <div id="case-study-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>
			    </div>
		    </div>
      </div>
    </div>
</div>
@endsection
