@if(isset($the_solutions))
	<section id="unifii-solutions" class="uni-solutions">
		<div class="wrap">
			<div class="container">
				<h3 class="title">Our Solutions</h3>
				<p class="subtitle">Discover our business solutions here at Unifii.</p>
				<div id="solutions-frame" class="row">				
					@foreach($the_solutions as $s)	
						<div class="col-md-6 solution-block {{ $s['slug'] }} solution-{{ $loop->iteration }}">
							<div class="inner blur-image">
								<div class="image">
								<div class="centred">
										<div class="centre">{!! $s['logo'] !!}</div>
								</div></div>
								<div class="overlay">
											<div class="wrapper">
												{!! $s['text'] !!}
											</div>							
								</div>
								<div class="link"><a href="{!! $s['link'] !!}" class="alt button" title="Find out more about {{ $s['title'] }}">Find out more</a></div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</section>
@endif
