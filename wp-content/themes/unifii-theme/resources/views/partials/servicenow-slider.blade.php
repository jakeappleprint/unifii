@if(isset($servicenow_slider))
  @php $c = count($servicenow_slider)*100 @endphp
  <section id="slider-frame" class="slider">
      <div class="frame">
        <div id="slider" class="rail row no-gutters" style="width: {{ $c }}%;">
          @foreach($servicenow_slider as $slide)
            @php $class = "slide col slide-" . $loop->index @endphp
            @if($loop->index === 0)
              @php $class.= ' current-slide' @endphp
            @endif
            <div class="{{ $class }}">
              {!! $slide['image'] !!}
              <div class="overlay slide-content">
                <div class="centred">
                  <div class="centre">
                    <div class="container">
                      {!! $slide['title'] !!}
                      <div class="wp-block-buttons">
                        <div class="wp-block-button"><a href="{!! $slide['url'] !!}">Discover more</a></div>
                        <div class="wp-block-button"><a href="#contact">Request a demo</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div id="slider-left" class="uni-nav uni-left-nav"><span><i class="fa fa-chevron-left"></i></span></div>
				<div id="slider-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>
      </div>
  </section>
@endif
