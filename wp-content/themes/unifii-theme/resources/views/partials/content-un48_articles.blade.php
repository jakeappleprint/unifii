<article id="content" @php post_class('articles entry-content') @endphp>
    <a href="@php the_permalink(); @endphp" class="" title="Find out more about {{ get_the_title() }}">@php the_title() @endphp</a>
</article>
