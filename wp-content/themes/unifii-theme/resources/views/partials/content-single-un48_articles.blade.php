<article @php post_class("unifii-post wrap") @endphp>
  <div class="container">
    <div class="entry-content">
      <header>

        <h1 class="title">{!! get_the_title() !!}</h1>

      </header>
      @php the_content() @endphp

    </div>
  </div>
</article>
