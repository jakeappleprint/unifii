<header id="masthead" class="site-header">
  @if(is_front_page())
    <h1 class="sr-only sr-focusable">{{ get_bloginfo('title') }}, {{ get_bloginfo('description') }}</h1>
  @endif
  <a href="#content" class="sr-only sr-focusable">Skip to content</a>
  <div class="container-fluid">
    <div class="row top-row no-gutters">
      <div class="col logo-col">
        <a class="brand" href="{{ home_url('/') }}">
          @if($site_logo)
            {!! $site_logo !!}
          @endif
        </a>
      </div>
      <div class="col nav-col">
        <nav class="nav-primary">
          <div id="mainMenu" class="nav-frame">
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav justify-content-center nav-fill']) !!}
            @endif
          </div>
        </nav>
        <div id="navToggle" class="nav-toggle"><span class="sr-only sr-focusable">Menu</span><span class="menu-icon"></span></div>
      </div>
      <div class="col contact-col">
        <div id="navContact" class="contact-info">
          <p>{!! $contact_info['email'] !!} {!! $contact_info['tel'] !!}</p>
        </div>
        <div class="header-search">
          @include('partials.searchform')
        </div>
      </div>
    </div>
  </div>
</header>
