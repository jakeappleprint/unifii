@if($contact_form) 
	<section id="contact" class="quick-contact-form">
		<div class="wrap">
			<div class="container contact-form-wrap">
				<h3 class="title">{{ $contact_form['ttl'] }}</h3>
				{!! $contact_form['frm'] !!}
			</div>
		</div>
	</section>
@endif