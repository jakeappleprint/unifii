<article @php post_class('unifii-page entry-content') @endphp>
    @php the_content() @endphp
</article>
