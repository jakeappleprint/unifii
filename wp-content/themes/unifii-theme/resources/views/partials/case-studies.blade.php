@if($case_studies)
@php $c = count($case_studies) @endphp
<section id="unifii-case-studies" class="uni-case-studies">
	<div class="wrap">
		<div class="container">
			<h3 class="title">Customer Success</h3>

			<div class="cs-wrapper">
			<div class="case-studies-slider">
				<div id="case-studies-frame" class="frame">
					<div id="case-study-rail" class="rail row no-gutters" style="width: {{ $c }}00%;">
						@foreach($case_studies as $p)
						<div class="un48-cs col">
							<div class="inner blur-image">
								<div class="image">{!! $p['image'] !!}</div>
								<div class="overlay">
									<div class="centred">
										<div class="centre">
											<img src="{!! $p['logo'] !!}" class="cs-logo" alt="{{ $p['title'] }}" />
</div>
									</div>
								</div>
								<div class="overlay link-overlay"><a href="{!! $p['link'] !!}" class="alt button">Find out more</a></div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				<div id="case-study-left" class="uni-nav uni-left-nav"><span><i class="fa fa-chevron-left"></i></span></div>
				<div id="case-study-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>
			</div>
		</div>
	</div></div>
</section>
@else
<h1>Works</h1>
@endif
