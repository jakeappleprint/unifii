<article @php post_class("unifii-post wrap") @endphp>
  <div class="container">
    <div class="entry-content">
      <header>
        <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
        <h1 class="title">{!! get_the_title() !!}</h1>
        @include('partials.entry-meta')
      </header>
      @php the_content() @endphp
    </div>
  </div>
</article>
