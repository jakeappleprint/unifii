<footer class="site-footer">
  <div class="wrap">
    <div class="container">
      <div class="row footer-widgets justify-content-between">
        <div class="col-12 logo-col">
          <a class="brand" href="{{ home_url('/') }}">
            @if($site_logo)
            {!! $site_logo !!}
            @endif
          </a>
        </div>
        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
    </div>
  </div>
  <div class="sub-footer">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-7 col-xl-6">
          <p>{!! $contact_info['email'] !!} <span>{!! $contact_info['tel'] !!}</span> <img src="@asset('images/marker.png')" class="marker"/> {!! $contact_info['address'] !!}</p>
        </div>
        <div class="col-md-5 col-xl-6">
          <nav id="footer-subnav" class="sub-nav">
            @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'sub_nav', 'menu_class' => 'nav']) !!}
            @endif
          </nav>
          <p class="no-margin"> &copy; {{ date('Y') }} {{ bloginfo('title') }}. All rights reserved</p>
        </div>
      </div>
  @if(is_active_sidebar('sidebar-quick'))
    <div id="quick-sidebar" class="quick-sidebar">
      <div id="quick-wrap" class="inner">
        <div id="close-sidebar" class="closey"><i class="fa fa-times"></i></div>
        @php dynamic_sidebar('sidebar-quick') @endphp
      </div>
      <div id="quick-sidebar-access" class="quick-sidebar-tab">
        <span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
      </div>
    </div>
  @endif
    </footer>

