@if(isset($main_slider))
  @php $c = count($main_slider)*100 @endphp
  <section id="slider-frame" class="slider">
    
      <div class="frame">
        <div id="slider" class="rail row no-gutters" style="width: {{ $c }}%;">
          @foreach($main_slider as $slide)
            @php $class = "slide col slide-" . $loop->index @endphp
            @if($loop->index === 0)   
              @php $class.= ' current-slide' @endphp
            @endif
            <div class="{{ $class }}">
              {!! $slide['image'] !!}
              <div class="overlay slide-content">
                <div class="centred">
                  <div class="centre">
                    <div class="container">
                      {!! $slide['content'] !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div id="slider-left" class="uni-nav uni-left-nav"><span><i class="fa fa-chevron-left"></i></span></div>
				<div id="slider-right" class="uni-nav uni-right-nav"><span><i class="fa fa-chevron-right"></i></span></div>   
      </div>
  </section>
@endif
