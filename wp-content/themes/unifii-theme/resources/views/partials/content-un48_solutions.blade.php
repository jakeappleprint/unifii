<div class="col-md-6 solution-block solution-{{ $x }}">
  @php $logo_id = get_post_meta(get_the_ID(), '_un48_logo_id', true); @endphp
  <div class="inner blur-image">
    <div class="image">
      <div class="centred">
          <div class="centre"><?= wp_get_attachment_image($logo_id, 'full-size', false, ['class' => 'blur-this']); ?></div>
      </div>
    </div>
    <div class="overlay">
      <div class="wrapper">
        @php the_excerpt(); @endphp
      </div>							
    </div>
    <div class="link"><a href="@php the_permalink(); @endphp" class="alt button" title="Find out more about {{ get_the_title() }}">Find out more</a></div>
  </div>
</div>
