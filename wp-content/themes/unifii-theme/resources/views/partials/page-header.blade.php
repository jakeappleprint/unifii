
@if($background_post_thumbnail)
<div id="home-background-image" class="banner background-image" {!! $background_post_thumbnail !!}>
  <div class="banner-content">
    @if($breadcrumbs)
    <div class="breadcrumb-wrap">
      <div class="container">{!! $breadcrumbs !!}</div>
    </div>
    @endif

    @if(!is_singular('post'))
    <div class="banner-wrap centred">
      <div class="centre">
        <div class="container-fluid">
          @if(is_archive())
          {{-- <div class="with-image">
            <img src="/wp-content/uploads/2020/03/unifii-wht-cutout.png" class="title-logo" alt="Unifii logo">
            <span></span>
            <h1 class="title">{!! App::title() !!}1</h1>
          </div>


          <div class="content">
                <h1>{!! $the_logo !!}</h1>
              </div> --}}
          @endif
          @if (isset($the_logo))

          @if(is_page('about-us') || is_page('company'))
          <h1 class="title">{!! App::title() !!} <span>{!! $the_logo !!}</span></h1>
          @elseif(is_page())
          <div class="{{ $class }}">


            <div class="with-image">

              <img src="/wp-content/uploads/2020/03/unifii-wht-cutout.png" class="title-logo" alt="Unifii logo">
              <span></span>
              <h1 class="title">{!! App::title() !!}</h1>

              <div class="content">
                <h1>{!! $the_logo !!}</h1>
              </div>

            </div>
          </div>

          @else
          <div class="content">
            <h1>{!! $the_logo !!}</h1>

            <!-- <div class="buttons">
                  <div class="button"><a href="#content">Discover more</a></div>
                  <div class="button"><a href="/contact-us">Request a demo</a></div>
                </div> -->

          </div>
          @endif
          @else
          <div class="with-image">
            <img src="/wp-content/uploads/2020/03/unifii-wht-cutout.png" class="title-logo" alt="Unifii logo">
            <span></span>
            <h1 class="title">{!! App::title() !!}</h1>
          </div>
          @endif


        </div>
      </div>
    </div>
    @endif
  </div>
</div>
@else
<div class="banner resource-banner background-image">
  <div class="banner-content">
    <div class="banner-wrap centred">
      <div class="centre">
        <div class="container-fluid">
          <div class="with-image resources-banner">
            <img src="/wp-content/uploads/2020/06/Unifii-Logo-01.png" class="title-logo" alt="Unifii logo">
            <span></span>
            <h1 class="title">{!! App::title() !!}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
