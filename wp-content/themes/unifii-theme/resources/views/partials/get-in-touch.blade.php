@if(isset($in_touch))
	<section id="unifii-cta" class="call-to-action overlay-banner un48-in-touch blur-image">
		<div class="wrap">
			<div class="background-image" {!! $in_touch['img'] !!}></div>
			<div class="container">
				<h3>{{ $in_touch['ttl'] }}</h3>
				<div class="text">{!! $in_touch['txt'] !!}</div>
				<div class="link"><a href="{!! $in_touch['lnk'] !!}" class="button">Find out more</a></div>
			</div>
		</div>
	</section>
@endif
