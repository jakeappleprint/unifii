@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  @include('partials.no-posts')

  <div class="wrap">
    <div class="container">
      <div id="post-grid" class="row">
        @while(have_posts()) @php the_post() @endphp
          @include('partials.content')
        @endwhile
    </div>
    @if(isset($load_more))
        <div class="row button-wrapper">
          <div class="col-6 offset-3"><button id="load-more-posts" class="button"{{ $load_more }}>Load more</button></div>
        </div>
      @endif
  </div>
</div>

@endsection

@section('after-main-content')
  @include('partials.latest-posts')
@endsection
