<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if(is_category()) {
            return single_cat_title( '', false );
        }
        if (is_archive()) {
            $postType = get_post_type_object(get_post_type());
            return $postType->labels->name;
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }


        return get_the_title();
    }


    public function siteLogo() {
        if(has_header_image()) {
            $alt = esc_attr( get_bloginfo( 'name' ) ) . ' Logo';
            $p = '<img id="siteLogo" src=" '. get_header_image() .'" alt="'. $alt .'" class="logo" />';
        } else {
            $p = '<h1>' . get_bloginfo('name') . '</h1>';
        }
        return  $p;
    }

    public function contactInfo() {
        $opts = get_option('un48_theme_opts');
        if( isset($opts['company_tel']) && !empty($opts['company_tel']) && isset($opts['company_email']) && !empty($opts['company_email'])) {
            return [
                'tel' => '<a href="tel:'. str_replace(" ", "", $opts['company_tel']) .'"><img src="'. \App\asset_path('images/tel.png') .'" />'. $opts['company_tel'] .'</a>',
                'email' => '<a href="mailto:'. $opts['company_email'] .'" class="email-link"><i class="fa fa-envelope-o"></i>'. $opts['company_email'] .'</a>',
                'address' => $opts['company_address'],
            ];
        }
    }

    public function caseStudies() {
        $args = [
            'post_type'      => 'un48_casestudies',
            'posts_per_page' => -1,
        ];
        $title = 'Latest News';
        $cs = get_posts($args);

        if($cs) {
            return array_map( function($post) {
                $logo_id = get_post_meta($post->ID, '_un48_logo_id', true);
                $logo = wp_get_attachment_image_src($logo_id, 'full-size');
                return [
                    'image' => get_the_post_thumbnail($post->ID, 'case-study', ['class' => 'blur-this']),
                    'link'   => get_permalink($post->ID),
                    'title' => $post->post_title,
                    'logo' => $logo[0],
                ];
            }, $cs);
        }
    }

    public function latestPosts(){
        $args = [
            'posts_per_page' => 12,
            'cat' => 5, // blogs
        ];
        $title = 'Our Blogs';
        if(is_category(4) || in_category(4, get_the_ID())) {
            $args['cat'] = 4;
            $title = 'Latest News';
        }
        if(is_singular('post')) {
            $args['exclude'] = get_the_ID();
        }
        $news = get_posts($args);
        if($news) {
            $posts = array_map(function($post){
                return [
                    'img' => get_the_post_thumbnail($post->ID, 'post-thumb'),
                    'ttl' => $post->post_title,
                    'lnk' => get_permalink($post->ID),
                ];
            }, $news);
            return [
                'title' => $title,
                'posts' => $posts,
            ];
        }
    }

    public function inTouch() {
        $opts = get_option('un48_theme_opts');
        if( isset($opts['get_in_touch']) && !empty($opts['get_in_touch'])) {
            $post = get_post($opts['get_in_touch']);
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');
            if(is_array($image)) {
                return [
                    'img' => 'style="background-image: url('. esc_url($image[0]) .')"',
                    'ttl' => $post->post_title,
                    'txt' => apply_filters('the_content', $post->post_content),
                    'lnk' => get_permalink($post->ID),
                ];
            }
        }
    }

    public function contactForm() {
        $opts = get_option('un48_theme_opts');
        if( isset($opts['main_contact']) && !empty($opts['main_contact'])) {
            $post = get_post($opts['main_contact']);

            if($post) {
                return [
                    'ttl' => $post->post_title,
                    'frm' => do_shortcode($post->post_content),
                ];
            }
        }
    }

    public function backgroundPostThumbnail(){
        if(is_category()) {
            $t = get_queried_object();
            /*if(is_singular('post')) {
                $cats = get_the_category($t->ID);
                $t = $cats[0];
            } */
            $thm = get_term_meta($t->term_id, '_cat_image_id', true);
        } else {
            $id = get_the_ID();
            if(is_archive() || is_search()) {
                $id = get_option('page_for_posts');
            }
            $thm = get_post_thumbnail_id($id);
        }
        $image = wp_get_attachment_image_src($thm, 'slide');
        if(is_array($image)) {
            return 'style="background-image: url('. esc_url($image[0]) .')"';
        }
    }

    public function theLogo() {
        global $post;
        if(isset($post) && is_object($post)) {
            $logo = get_post_meta($post->ID, '_un48_logo_id', true);
            $headline = get_post_meta($post->ID, '_un48_banner_txt', true);
            if($logo) {
                $html = wp_get_attachment_image($logo, 'full-size', false, ['class' => 'title-logo', 'alt' => esc_attr($post->post_title)]);
                if($post->post_parent || 'un48_services' === $post->post_type){
                    $html = '<div class="with-image services-solutions">'  . $html . '<br/><br/><h1 class="title">' . App::title() .'</h1></div>' . '<div class="buttons">
                      <div class="button"><a href="#content">Discover more</a></div>
                      <div class="button"><a href="/contact-us">Request a demo</a></div>
                    </div>';
                } else if('un48_solutions' === $post->post_type && !$post->post_parent) {
                    $opts = get_option('un48_theme_opts');
                    $company_logo = '';
                    if(isset($opts['company_logo'])) {
                        $company_logo =  wp_get_attachment_image($opts['company_logo'], 'full-size', false, ['class' => 'company-logo', 'alt' => esc_attr(get_bloginfo('title'))]);
                    }
                    $html = '<div class="with-image sub-services-solutions"><h1 class="sr-only sr-focusable">'.App::title().'</h1>'. $company_logo .'<span></span>'. $html .'</div>';
                    $html.= '<p class="headline">'. $headline .'</p>';
                    if('talkdesk-contact-centre-software' !== $post->post_name) {
                        $html.= '<div class="wp-block-buttons"><a href="#content" class="button">Discover more</a> <a href="'. home_url('contact/') .'" class="button">Request a demo</a></div>';
                    } else {
                        $html.= '<div class="wp-block-buttons"><a href="#" class="button watch-video-link">Watch video</a> <a href="'. home_url('contact/') .'" class="button">Request a demo</a></div>';
                        $html.= '<div id="talkdesk-video" class="video-screen"><script src="https://fast.wistia.com/embed/medias/nmyqgizz6i.jsonp" async=""></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_nmyqgizz6i popover=true videoFoam=true playButton=false" style="height:100%;position:relative;width:100%">&nbsp;</div></div></div></div>';
                    }

                } else if($post->post_parent || 'un48_casestudies' === $post->post_type){
                    $html = '<div class="with-image customer-success"><img src="/wp-content/uploads/2020/03/unifii-wht-cutout.png" class="title-logo" alt="Unifii logo">
                    <span></span>
                    <h1 class="title">'. App::title() . '</h1></div>';
                }

                else if(!empty($headline)) {
                    $html.= '<p class="headline">'. $headline .'</p>';
                }
            } else if(!empty($headline) && is_page()){
                $html =  $headline;
            }
        }
        if(isset($html)){
            return $html;
        }
    }

    public function breadcrumbs(){
        global $post;
        $bc = [
            [ 'title' => 'Home',
              'url' => home_url(),
            ],
        ];
        $html = '<p class="breadcrumbs">';
        if('un48_services' === get_post_type() || 'un48_solutions' === get_post_type()) {
            $postType = get_post_type_object(get_post_type());
            if ($postType) {
                $bc[] = [
                    'title' => $postType->labels->name,
                    'url' => get_post_type_archive_link(get_post_type()),
                ];

            }
            if(is_singular('un48_solutions') && $post->post_parent) {
                $bc[] = [
                    'title' => get_the_title($post->post_parent),
                    'url' => get_permalink($post->post_parent),
                ];
                $initials = explode(" ", get_the_title());
                $title = '';
                foreach($initials as $i) {
                    $l = 1;
                    if(2 === strlen($i)){
                        $l = 2;
                    }
                    $title.= substr($i, 0, $l);
                }
                if($post->post_name == 'health-check') {
                    $title = get_the_title();
                } else if($post->post_name === 'security-operations') {
                    $title = 'SecOps';
                }
                $bc[] = ['title' => $title ];
            } else if(!is_archive()) {
                $bc[] = ['title' => get_the_title() ];
            }
        } else if(is_category() || is_singular(['post', 'un48_casestudies'])) {
            $cat = get_the_category();
            foreach($cat as $t) {
                if($t->parent) {
                    $bc[] = [
                        'title' => get_cat_name($t->parent),
                        'url' => get_term_link($t->parent),
                    ];
                }
                $bc[] = [
                    'title' => $t->name,
                    'url' => get_term_link($t->term_id),
                ];
            }
        } else if(is_single() && !is_singular('post')) {
          $bc[] = ['title' => get_the_title() ];
        } else if(is_page()) {
            if($post->post_parent) {
                $bc[] = [
                    'title' => get_the_title($post->post_parent),
                    'url' => get_permalink($post->post_parent),
                ];
            } else if(is_page('faqs-glossary')) {
                $bc[] = [
                    'title' => 'Resources',
                    'url' => get_term_link('resources', 'category'),
                ];
            }
            $title = get_the_title();
            if(is_page('careers') || is_page('company')) {
                $title = ucfirst($post->post_name);
            }
            $bc[] = ['title' => $title ];

        }

        $x=0;
        foreach($bc as $b) {
            $x++;
            if(isset($b['url'])) {
                $html.= '<a href="'. $b['url'] .'" rel="nofollow">'. $b['title'] .'</a>';
            } else {
                $html.= '<span>'. $b['title'] .'</span>';
            }
            if(count($bc) > $x ) {
                $html.='<i class="fa fa-angle-right" aria-hidden="true"></i>';
            }
        }
        $html.='</p>';
        return $html;

    }
}
