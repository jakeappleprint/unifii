<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
  public function theServices() {
    $args = [
      'post_type'      => 'un48_services',
      'posts_per_page' => 4,
      'exclude'        => get_the_ID()
    ];
    $services = get_posts($args);
    if($services) {
      return array_map( function($post) {
        $image = get_post_meta($post->ID, '_un48_slogo_id', true);
        if(!empty($image)) {
          $image = wp_get_attachment_image($image, 'square', false, ['class' => 'uni-service-image' ]);
        }
        $title = get_post_meta($post->ID, '_un48_banner_txt', true);
        if(empty($title)) {
          $title = $post->post_title;
        }

        return [
          'text'   => wpautop($post->post_excerpt),
          'image' => $image,
          'link'   => get_permalink($post->ID),
          'title' => $title,
        ];
      }, $services);
    }
  }

  public function theSolutions() {
    $args = [
      'post_type'      => 'un48_solutions',
      'posts_per_page' => 4,
      'exclude'        => get_the_ID(),
      'post_parent'    => 0,
      'orderby'        => 'date',
    ];
    $solutions = get_posts($args);

    if($solutions) {
      return array_map( function($post) {
        $logo_id = get_post_meta($post->ID, '_un48_logo_id', true);
        return [
          'text' => wpautop($post->post_excerpt),
          'logo' => wp_get_attachment_image($logo_id, 'full-size', false, ['class' => 'blur-this']),
          'link' => get_permalink($post->ID),
          'slug' => $post->post_name,
          'title' => $post->post_title,
        ];
      }, $solutions);
    }
  }

  public function mainSlider(){
    $args = array(
      'posts_per_page'    => -1,
      'post_type'         => 'un48_slides',
    );
    $slides = get_posts($args);
    if($slides) {
      return array_map( function ($slide) {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'slide');
        $logo = get_post_meta($slide->ID, '_un48_logo_id', true);

        $logo = wp_get_attachment_image_src($logo, 'slide');
        $content = '<img src="'. $logo[0] .'" class="slide-logo" />';
        $opts = get_option('un48_theme_opts');
        $company_logo = '';

        if( $slide->ID == 746 ) {
          $content = '<div class="two-logos with-image">' . $content . '</div>';
        }

        elseif($logo) {

          if(isset($opts['company_logo'])) {
            $company_logo =  wp_get_attachment_image($opts['company_logo'], 'full-size', false, ['class' => 'company-logo', 'alt' => esc_attr(get_bloginfo('title'))]);
            $content = '<div class="two-logos with-image">' . $content . '<span></span>' . $company_logo . '</div>';

          }


        }


        else {
          $content = '<h2>'. $slide->post_title . '</h2>';
        }
        return [
          'content' => $content . $slide->post_content,
          'image' => '<div class="bg-image" style="background-image: url('. $img[0] .');"></div>',
        ];
      }, $slides );
    }
  }
}
