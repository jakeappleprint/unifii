<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller
{
    public function theGravatar(){
        global $post;
        if($avatar = get_avatar($post->post_author, 64 )) {
            return $avatar;
        }
        
    }

    public function theJobDesc(){
        global $post;
        $jd = get_user_meta( $post->post_author, '_un48_jobdesc', true);
        if(!empty($jd)) {
            return $jd;
        }
    }
}
