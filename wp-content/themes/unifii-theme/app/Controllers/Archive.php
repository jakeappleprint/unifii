<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Archive extends Controller
{
     public function loadMore() {
        global $wp_query;
        if($wp_query->max_num_pages > 1) {
            $resp = ' data-paged=1 data-ncheck=' . wp_create_nonce('ajaxloadposts');
            if(is_category()) {
                $resp.= ' data-cat='. get_query_var('cat');
            }
            return $resp;
        }
    }

}
