<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    global $post;
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    if(isset($post) && $post->post_parent) {
        $classes[] = 'subpage';
    }

    if(get_post_type() === 'un48_casestudies' || is_front_page()) {
        $classes[] = 'unifii-case-studies';
    }
    if(is_single('certified-servicenow-partner') || is_front_page()) {
        $classes[] = 'unifii-slider';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip;';
});

/**
 *  Change number of words displayed in the excerpt
 */
add_filter( 'excerpt_length', function ( $length ) {
    return 28;
}, 999 );
 
/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 *
 *  Add extra sizes to Image size chooser
 *  
**/
add_filter( 'image_size_names_choose', function($sizes){
    return array_merge( $sizes, array(
        'slide' => __('Banner'),
        'image-slider' => __('Image Slider'),
    ) );
});

/**
 *
 *  Create Google Maps
 *  
**/
add_shortcode('map', function($attr, $content="") {
    $opts = get_option('un48_theme_opts');
    if(!empty($content) && isset($opts['google_mapkey'])) {
        $embed_args = array(
            'key'	=> $opts['google_mapkey'],
            'q'		=> $content,
            'zoom'	=> 16,
        );
        $src = 'https://www.google.com/maps/embed/v1/place?' . http_build_query($embed_args);
        return '<iframe src="' . $src . '" class="map-frame" allowfullscreen></iframe>';
    }
});

/**
 *  remove "Private" from titles
**/
add_filter( 'private_title_format',function ( $format ) {
    return "%s";
});

/**
*  remove "Archive" from titles
**/
add_filter( 'get_the_archive_title', function () {
   return '';
}, 99);


/**
*  remove sub solutions from query
**/
add_filter( 'pre_get_posts', function( $query ) {
    if (!is_admin() && $query->is_main_query() && is_archive() && false == $query->query_vars['post_parent'] && isset($query->query_vars['post_type']) && $query->query_vars['post_type'] === 'un48_solutions') {
        $query->set( 'post_parent', 0 );
    }
}, 99);

/**
 *  Add data attribute to gator script
 */

 add_filter('script_loader_tag', function($tag, $handle) {
    if('gator' !== $handle) return $tag;

    return str_replace(' src', ' data-cfasync="false" src', $tag);
 }, 10, 2);

 /**
  *     Add button to menu items with children
  */
  add_filter( 'walker_nav_menu_start_el', function($item_output, $item, $depth, $args){
    if(is_array($item->classes) && in_array('menu-item-has-children', $item->classes)) {
        $fa = 'fa fa-chevron-down';
        if($depth > 0) {
            $fa = 'fa fa-chevron-right';
        }
        $item_output.= '<span class="submenu-toggle"><i class="'. $fa .'"  aria-hidden="true"></i></span>';
    }
    return $item_output;
  }, 10, 4);