<?php 

namespace App;

// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add Job Description to user meta
 */
function un48_user_meta($user){ 
	if('add-new-user' != $user) {
		$jobdesc = get_user_meta($user->ID, '_un48_jobdesc', true);
		if(empty($jobdesc)) { $jobdesc=''; }
	} else {
		$jobdesc = '';
	}  ?>
	<table class="form-table">
		<tr>
			<th><label for="job-desc">Job Desc</label></th>
			<td><input type="text" class="regular-text" name="un48-job-desc" value="<?= $jobdesc; ?>" placeholder="Job Description"/> </td>
		</tr>
	</table><?php
}
add_action( 'show_user_profile', __NAMESPACE__ . '\\un48_user_meta', 10, 1 );
add_action( 'edit_user_profile', __NAMESPACE__ . '\\un48_user_meta', 10, 1 );
add_action( 'user_new_form', __NAMESPACE__ . '\\un48_user_meta', 10, 1 );

/**
 * Save user Job Description data
 */
function save_un48_user_meta($user_id){
	if ( current_user_can( 'edit_user', $user_id ) ){
		update_user_meta($user_id,'_un48_jobdesc',$_POST['un48-job-desc']);
	}
}
add_action( 'personal_options_update', __NAMESPACE__ . '\\save_un48_user_meta' );
add_action( 'edit_user_profile_update', __NAMESPACE__ . '\\save_un48_user_meta' );
add_action( 'user_register', __NAMESPACE__ . '\\save_un48_user_meta' );

/**
 * Add homepage options metabox
 */
add_action('do_meta_boxes', function (){
	add_meta_box('uni-logo', 'Logo', __NAMESPACE__ . '\\uni_logo_metabox', ['un48_services', 'un48_solutions', 'un48_casestudies', 'un48_slides'], 'side', 'low' );
	add_meta_box('uni-menu-txt', 'Extra Text Content', __NAMESPACE__ . '\\uni_nav_menutxt', ['page','un48_solutions', 'un48_services'], 'side', 'low' );
	add_meta_box('uni-slogo', 'Homepage Image', __NAMESPACE__ . '\\uni_service_img_metabox', ['un48_services'], 'side', 'low' );
});


/**
 * Save post actions
 */  

add_action( 'save_post', function ( $id ) {
	if($id && isset($_POST['un48-logo-id']) ) {
		update_post_meta($id, '_un48_logo_id', intval($_POST['un48-logo-id']));
	}
	if($id && isset($_POST['un48-navtxt']) ) {
		update_post_meta($id, '_un48_navtxt', sanitize_text_field($_POST['un48-navtxt']));
	}
	if($id && isset($_POST['un48-banner-txt']) ) {
		update_post_meta($id, '_un48_banner_txt', sanitize_text_field($_POST['un48-banner-txt']));
	}
	if($id && isset($_POST['un48-slogo-id']) ) {
		update_post_meta($id, '_un48_slogo_id', intval($_POST['un48-slogo-id']));
	}
});

function uni_nav_menutxt($post){
	$c = get_post_meta($post->ID, '_un48_navtxt', true);
	$b = get_post_meta($post->ID, '_un48_banner_txt', true);
	?>
	<div class="row">
		<label>Menu Text Snippet</label>
		<input class="regular-text" type="text" name="un48-navtxt" id="un48-navtxt" value="<?= $c; ?>" style="max-width:100%;" />
	</div>
	<div class="row">
		<label>Banner Headline</label>
		<input class="regular-text" type="text" name="un48-banner-txt" id="un48-banner-txt" value="<?= $b; ?>" style="max-width:100%;" />
	</div>
	<?php
}

function uni_logo_metabox($post) {
	wp_enqueue_media();
 	$title = 'Logo';
 	$value = get_post_meta($post->ID, '_un48_logo_id', true);
 	$upload_link = esc_url( get_upload_iframe_src( 'image', $post->ID) );
	
	if(!empty($value)) {
		$value_src = wp_get_attachment_image_src( $value, 'medium' );
	} else {
		$value_src = false; 
	}	 ?>
	<div class="ski-container">

			<?php	// Get the image src
			$img_exists = is_array($value_src); ?>
			<div id="current-un48-logo"  style="background-color: #bababa; text-align: center; padding: 12px;">
			    <?php if ( $img_exists ) : ?>
			        <img src="<?php echo $value_src[0] ?>" alt="" style="max-width:100%;" />
			    <?php endif; ?>
			</div>
			<!-- Your add & remove image links -->
			<p class="hide-if-no-js">
			    <a class="upload-un48-logo <?php if ( $img_exists  ) { echo 'hidden'; } ?>" 
			       href="<?php echo $upload_link ?>">
			        <?php _e('Choose Logo') ?>
			    </a>
			    <a class="delete-un48-logo <?php if ( ! $img_exists  ) { echo 'hidden'; } ?>" 
			      href="#">
			        <?php _e('Remove this logo') ?>
			    </a>
			</p>			
			<input id="un48-logo-id" name="un48-logo-id" type="hidden" value="<?php echo esc_attr( $value ); ?>"/>
			<script>
			jQuery(function($){

		  // Set all variables to be used in scope
		  var frame,
		      metaBox = $('#uni-logo'), // Your meta box id here
		      addImgLink = metaBox.find('.upload-un48-logo '),
		      delImgLink = metaBox.find( '.delete-un48-logo '),
		      imgContainer = metaBox.find( '#current-un48-logo'),
		      imgIdInput = metaBox.find( '#un48-logo-id' );
		  
		  // ADD IMAGE LINK
		  addImgLink.on( 'click', function( event ){    
		    event.preventDefault();
		    
		    // If the media frame already exists, reopen it.
		    if ( frame ) {
		      frame.open();
		      return;
		    }
		    
		    // Create a new media frame
		    frame = wp.media({
		      title: 'Select or Upload an Icon',
		      button: {
		        text: 'Use this media'
		      },
		      multiple: false  // Set to true to allow multiple files to be selected
		    });

		    
		    // When an image is selected in the media frame...
		    frame.on( 'select', function() {      
		      // Get media attachment details from the frame state
		      var attachment = frame.state().get('selection').first().toJSON();
		      // Send the attachment URL to our custom image input field.
		      imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );
		      // Send the attachment id to our hidden input
		      imgIdInput.val( attachment.id );
		      // Hide the add image link
		      addImgLink.addClass( 'hidden' );
		      // Unhide the remove image link
		      delImgLink.removeClass( 'hidden' );
		    });

		    // Finally, open the modal on click
		    frame.open();
		  });
		  
		  
		  // DELETE IMAGE LINK
		  delImgLink.on( 'click', function( event ){
		    event.preventDefault();
		    // Clear out the preview image
		    imgContainer.html( '' );
		    // Un-hide the add image link
		    addImgLink.removeClass( 'hidden' );
		    // Hide the delete image link
		    delImgLink.addClass( 'hidden' );
		    // Delete the image id from the hidden input
		    imgIdInput.val( '' );
		  });

		});
			</script>


	</div>
	<?php	
}

function uni_service_img_metabox($post) {
	wp_enqueue_media();
 	$title = 'Service Graphic';
 	$value = get_post_meta($post->ID, '_un48_slogo_id', true);
 	$upload_link = esc_url( get_upload_iframe_src( 'image', $post->ID) );
	
	if(!empty($value)) {
		$value_src = wp_get_attachment_image_src( $value, 'medium' );
	} else {
		$value_src = false; 
	}	 ?>
	<div class="ski-container">

			<?php	// Get the image src
			$img_exists = is_array($value_src); ?>
			<div id="current-un48-slogo"  style="background-color: #bababa; text-align: center; padding: 12px;">
			    <?php if ( $img_exists ) : ?>
			        <img src="<?php echo $value_src[0] ?>" alt="" style="max-width:100%;" />
			    <?php endif; ?>
			</div>
			<!-- Your add & remove image links -->
			<p class="hide-if-no-js">
			    <a class="upload-un48-slogo <?php if ( $img_exists  ) { echo 'hidden'; } ?>" 
			       href="<?php echo $upload_link ?>">
			        <?php _e('Choose Image') ?>
			    </a>
			    <a class="delete-un48-slogo <?php if ( ! $img_exists  ) { echo 'hidden'; } ?>" 
			      href="#">
			        <?php _e('Remove') ?>
			    </a>
			</p>			
			<input id="un48-slogo-id" name="un48-slogo-id" type="hidden" value="<?php echo esc_attr( $value ); ?>"/>
			<script>
			jQuery(function($){

		  // Set all variables to be used in scope
		  var frame,
		      metaBox = $('#uni-slogo'), // Your meta box id here
		      addImgLink = metaBox.find('.upload-un48-slogo '),
		      delImgLink = metaBox.find( '.delete-un48-slogo '),
		      imgContainer = metaBox.find( '#current-un48-slogo'),
		      imgIdInput = metaBox.find( '#un48-slogo-id' );
		  
		  // ADD IMAGE LINK
		  addImgLink.on( 'click', function( event ){    
		    event.preventDefault();
		    
		    // If the media frame already exists, reopen it.
		    if ( frame ) {
		      frame.open();
		      return;
		    }
		    
		    // Create a new media frame
		    frame = wp.media({
		      title: 'Select or Upload an Image',
		      button: {
		        text: 'Use this media'
		      },
		      multiple: false  // Set to true to allow multiple files to be selected
		    });

		    
		    // When an image is selected in the media frame...
		    frame.on( 'select', function() {      
		      // Get media attachment details from the frame state
		      var attachment = frame.state().get('selection').first().toJSON();
		      // Send the attachment URL to our custom image input field.
		      imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );
		      // Send the attachment id to our hidden input
		      imgIdInput.val( attachment.id );
		      // Hide the add image link
		      addImgLink.addClass( 'hidden' );
		      // Unhide the remove image link
		      delImgLink.removeClass( 'hidden' );
		    });

		    // Finally, open the modal on click
		    frame.open();
		  });
		  
		  
		  // DELETE IMAGE LINK
		  delImgLink.on( 'click', function( event ){
		    event.preventDefault();
		    // Clear out the preview image
		    imgContainer.html( '' );
		    // Un-hide the add image link
		    addImgLink.removeClass( 'hidden' );
		    // Hide the delete image link
		    delImgLink.addClass( 'hidden' );
		    // Delete the image id from the hidden input
		    imgIdInput.val( '' );
		  });

		});
			</script>


	</div>
	<?php	
}


